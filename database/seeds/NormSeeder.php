<?php

use Illuminate\Database\Seeder;

class NormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('norms')->insert([
			'name'       => 'ISO 9001:2015',
        ]);

        DB::table('norms')->insert([
			'name'       => 'ISO 14001:2015',
        ]);

        DB::table('norms')->insert([
			'name'       => 'ISO 22000:2018',
        ]);

        DB::table('norms')->insert([
			'name'       => 'ISO 45001:2018',
        ]);

    }
}
