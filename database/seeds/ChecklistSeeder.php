<?php

use Illuminate\Database\Seeder;

class ChecklistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// checklist norma  9
        $company = DB::table('checklists')->insert([
			'code'                 => '4.1',
			'description'          => 'Comprensión del contexto de la organización ¿Cómo determina las cuestiones externas e internas pertinentes para su propósito y su dirección estratégica que afectan el logro de objetivos de su SGC? ',
			'order'                => '1',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);

        $company = DB::table('checklists')->insert([
			'code'                 => '4.2',
			'description'          => 'Comprensión de las necesidades y expectativas de las partes interesadas ¿Cuáles partes interesadas son pertinentes para su sistema de gestión de la calidad? ¿Cuáles requisitos de las partes interesadas son pertinentes para el SGC? ¿Cómo da seguimiento y revisa la información pertinente de los requisitos de las partes interesadas y sus requisitos pertinentes?',
			'order'                => '2',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4.3',
			'description'          => 'Determinación del alcance del SGC ¿Cómo determinó los limites y aplicabilidad de su SGC? ¿Cómo considero los siguientes puntos para determinar el alcance de su SGC? a) las cuestiones externas e internas indicadas en el  apartado 4.2 b) Los requisitos de las partes interesadas pertinentes c) Los productos y servicios de la organización ¿Dónde se tiene disponible como información documentada el alcance del SGC? ¿Cuáles productos y/o servicios están considerados en el alcance del SGC? ¿Cuáles requisitos se excluyeron del SGC?',
			'order'                => '3',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4.4.1',
			'description'          => 'Sistema de gestión de calidad y sus procesos ¿Como se ha establecido, implementado y mantenido su SGC  icluyendo procesos e interracciones necesarios? ¿Cómo determinó los procesos necesarios y su interacción en el SGC incluyendo lo siguiente? a) determinación de las entradas requeridas y los resultados esperados de estos  procesos; b) determinación de la secuencia e interacción de estos procesos; c) determinación  y aplicación de  los criterios y los métodos (incluyendo el seguimiento, las mediciones y los indicadores del desempeño relacionados) necesarios para asegurarse de la operación eficaz y el control de estos procesos; d) determinación de los recursos necesarios para estos procesos y  su  disponibilidad; e) asignación de las responsabilidades y autoridades para estos procesos;  f) abordar los riesgos y oportunidades determinados de acuerdo con los requisitos del apartado 6.1; g) evaluación de estos procesos e implementar cualquier cambio necesario para asegurarse de que estos procesos logran los resultados previstos; h) mejora de los procesos y el sistema de gestión de la calidad',
			'order'                => '4',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4.4.2',
			'description'          => '¿Cómo mantiene  la información documentada que apoya la operación de sus procesos? ¿Cómo retiene la información documentada para tener confianza de que los procesos son llevados a cabo como se han planeado?',
			'order'                => '5',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '5.1.1',
			'description'          => 'Liderazgo y compromiso General ¿Cómo demuestra, la alta dirección, su liderazgo y compromiso con respecto a: a)  la responsabilidad y obligación de rendir cuentas con relación a la eficacia del SGC; b) establecimiento de la política d y los objetivos de la calidad para el SGC, y que éstos sean compatibles con el contexto y la dirección estratégica de la organización; c) asegurándo la integración de los requisitos del SGC en los procesos de negocio de la organización; d)  el uso del enfoque a procesos y el pensamiento basado en riesgos; e) asegurándo que los recursos necesarios para el SGC estén disponibles; f) Comunicando la importancia de una gestión de la calidad eficaz y conforme con los requisitos del SGC; g) asegurándo  que el SGC logre los resultados  previstos; h) Comprometiendo, dirigiendo y apoyando a las personas, para contribuir a la eficacia del SGC. i) Promoviendo la mejora; j) Apoyando otros roles pertinentes de la dirección, para demostrar su liderazgo en la forma en la que aplique a sus áreas de responsabilidad.',
			'order'                => '6',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '5.1.2',
			'description'          => 'Enfoque al cliente ¿Cómo demuestra, la alta dirección, su liderazgo y compromiso con respecto a que: a) se determinen, se comprenden y se cumplen regularmente los requisitos del cliente y los legales y reglamentarios aplicables; b) se determinen y se consideran los riesgos y oportunidades que pueden afectar a la conformidad de los productos y servicios y a la capacidad de aumentar la satisfacción del cliente; c) se mantenga el enfoque en el aumento de la satisfacción del cliente',
			'order'                => '7',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '5.2.1',
			'description'          => 'Política Establecimiento de la política de calidad ¿Cómo establece, implementa y mantiene una politica de calidad basando en que: a) sea apropiada al propósito y contexto de la organización y apoye su dirección estratégica; b) proporcione un marco de referencia para el establecimiento de los objetivos de la calidad; c) incluya un compromiso de cumplir los requisitos aplicables; d) incluya un compromiso de mejora continua del sistema de gestión de la calidad.',
			'order'                => '8',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '5.2.2',
			'description'          => 'Comunicación de la política de calidad ¿Cómo  asegura la correcta comunicación de la politica de calidad? considerando que debe: a) estar disponible y mantenerse como información documentada; b) comunicarse, entenderse y aplicarse dentro de la organización; c) estar disponible para las partes interesadas pertinentes, según corresponda',
			'order'                => '9',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '5.3',
			'description'          => 'Organización, roles, responsabilidades y autoridades ¿Cómo se han asignado responsabilidades y autoridades para: a) asegurarse de que el SGC es conforme con los requisitos de esta Norma Internacional; b) asegurarse de que los procesos están generando y proporcionando los resultados previstos; c) informar, en particular, a la alta dirección sobre el desempeño del SGC y sobre las oportunidades de mejora (véase 10.1); d) asegurarse de que se promueve el enfoque al cliente en toda la organización; e) asegurarse de que la integridad del SGC se mantiene cuando se planifican e implementan cambios SGC',
			'order'                => '10',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '6.1.1',
			'description'          => 'Acciones para abordar riesgos y oportunidades ¿Cómo se determinaron riesgos y oportunidades necesarios para: a) asegurar que el SGC logre los resultados  previstos; b) aumente los efectos deseables; c) prevenga o reduzcar efectos no deseados; d) Logre la mejora?',
			'order'                => '11',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '6.1.2',
			'description'          => '¿ Cómo se planifican las acciones para:poder abordar riesgos y oportunidades al igual que: 1) integrar e implementar las acciones en sus procesos del SGC;2) evaluar la eficacia de estas acciones?',
			'order'                => '12',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '6.2.1',
			'description'          => 'Objetivos de calidad y planeación para alcanzarlos ¿Cuales objetivos se tienen para cada función y nivel pertinente y de los procesos necesarios para el SGC? ¿Estos objetivos cumplen con las siguientes caracteristicas: a) ser coherentes con la política de la calidad; b) ser medibles; c) tener en cuenta los requisitos aplicables; d) ser pertinentes para la conformidad de los productos y servicios y para el aumento de la satisfacción del cliente; e) ser objeto de seguimiento; f) comunicarse; g) actualizarse, según corresponda?',
			'order'                => '13',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '6.2.2',
			'description'          => '¿ Cómo se  planifica el logro de sus objetivos de calidad con base en: a) qué se va a hacer; b) qué recursos se requerirán; c) quién será responsable; d) cuándo se finalizará; e) cómo se evaluarán los resultados?',
			'order'                => '14',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '6.3',
			'description'          => 'Planeación del cambio 	¿Cómo se planean los cambios necesarios en el SGC  considerando lo siguiente: a)el propósito de los cambios y sus consecuencias potenciales; b)la integridad del sistema de gestión de la calidad; c)la disponibilidad de recursos; d)la asignación o reasignación de responsabilidades y autoridades?',
			'order'                => '15',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '16',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '17',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '18',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '19',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '',
			'description'          => 'Contexto de la organización',
			'order'                => '20',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '21',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '22',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);
        $company = DB::table('checklists')->insert([
			'code'                 => '4',
			'description'          => 'Contexto de la organización',
			'order'                => '23',
			'company_sector_id'    => '4',
			'section_checklist_id' => '4',
			'type'                 => '4',
        ]);


    }
}
