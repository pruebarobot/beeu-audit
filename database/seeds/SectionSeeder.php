<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	$company = DB::table('section_checklists')->insert([
			'code'        => '4',
			'description' => 'Contexto de la organización',
			'order'       => '4',
        ]);
    	
    	$company = DB::table('section_checklists')->insert([
			'code'        => '5',
            'description' => 'Liderazgo y participación de los trabajadores',
			// 'description' => 'Liderazgo ',
			'order'       => '5',
        ]);

    	$company = DB::table('section_checklists')->insert([
			'code'        => '6',
            'description' => 'Planeación', 
			// 'description' => 'Planificación', 
			'order'       => '6',
        ]);
        $company = DB::table('section_checklists')->insert([
			'code'        => '7',
            'description' => 'Apoyo',
			// 'description' => 'Apoyo',
			'order'       => '7',
        ]);
        $company = DB::table('section_checklists')->insert([
			'code'        => '8',
            'description' => 'Operación',
			// 'description' => 'Operación',
			'order'       => '8',
        ]);
        $company = DB::table('section_checklists')->insert([
			'code'        => '9',
            'description' => 'Evaluación del desempeño',
			// 'description' => 'Evaluación del desempeño',
			'order'       => '9',
        ]);
        $company = DB::table('section_checklists')->insert([
			'code'        => '10',
            'description' => 'Mejora',
			// 'description' => 'Mejora',
			'order'       => '10',
        ]);
             
    }
}
