<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditSectorNormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_sector_norms', function (Blueprint $table) {
            $table->id();
            $table->string('sector_id')->nullable();
            $table->string('norm_id')->nullable();
            $table->unsignedBigInteger('audit_id');
            $table->date('init_date')->nullable();
            $table->date('finish_date')->nullable();
            $table->foreign('audit_id')->references('id')->on('audits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_sector_norms');
    }
}
