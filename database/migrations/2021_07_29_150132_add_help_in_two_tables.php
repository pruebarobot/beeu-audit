<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHelpInTwoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->text('help')->nullable();
        });

        Schema::table('result_checklists', function (Blueprint $table) {
            $table->text('help')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->dropColumn('checklists');
        });

        Schema::table('result_checklists', function (Blueprint $table) {
            $table->dropColumn('checklists');
        });
    }
}
