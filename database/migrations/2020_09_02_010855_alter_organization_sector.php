<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOrganizationSector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_sector_norms', function ($table) {
            $table->dropForeign('organization_sector_norms_norm_id_foreign');
            $table->dropForeign('organization_sector_norms_sector_id_foreign');
            $table->dropColumn('sector_id');
            $table->dropColumn('norm_id');
            $table->unsignedBigInteger('company_sector_id')->after( 'organization_id' );
            $table->foreign('company_sector_id')->references('id')->on('company_sectors');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
