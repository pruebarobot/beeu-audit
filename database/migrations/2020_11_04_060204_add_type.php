<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->enum('type', ['Mayor', 'Menor','NA'])->nullable()->after('description');
        });

        Schema::table('result_checklists', function (Blueprint $table) {
            $table->enum('type', ['Mayor', 'Menor','NA'])->nullable()->after('description');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('result_checklists', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
