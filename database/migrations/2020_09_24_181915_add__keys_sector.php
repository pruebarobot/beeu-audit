<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeysSector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_sector_norms', function ($table) {
            $table->unsignedBigInteger('norm_id')->after( 'id' );
            $table->unsignedBigInteger('sector_id')->after( 'id' );
            $table->foreign('sector_id')->references('id')->on('sectors');
            $table->foreign('norm_id')->references('id')->on('norms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
