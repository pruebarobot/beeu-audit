<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAuditIdToSectionresult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('result_section', function (Blueprint $table) {
            $table->unsignedBigInteger('audit_id')->after( 'description' );
            $table->foreign('audit_id')->references('id')->on('audits');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('result_section', function (Blueprint $table) {
            $table->dropForeign(['audit_id']);
            $table->dropColumn('audit_id');
        });
    }
}
