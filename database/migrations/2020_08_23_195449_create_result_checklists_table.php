<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_checklists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('audit_sector_norm_id');
            $table->string('conform')->nullable();
            $table->longText('observation')->nullable();
            $table->string('code')->nullable();
            $table->longText('description')->nullable();
            $table->foreign('audit_sector_norm_id')->references('id')->on('audit_sector_norms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_checklists');
    }
}
