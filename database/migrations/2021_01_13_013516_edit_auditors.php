<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditAuditors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_academics', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('pass');
            $table->dropColumn('date');
        });

        Schema::table('history_academics', function ($table) {
            $table->string('institution')->nullable()->after('id');
            $table->string('carrier')->nullable()->after('institution');
            $table->string('date')->nullable()->after('institution');
            $table->string('certification_institution')->nullable()->after('file_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_academics', function ($table) {
            $table->dropColumn('institution');
            $table->dropColumn('carrier');
            $table->dropColumn('date');
            $table->dropColumn('certification_institution');
        });

        Schema::table('audits', function ($table) {
            $table->string('pass')->nullable()->after('id');
            $table->string('name')->nullable()->after('id');
            $table->string('date')->nullable()->after('id');
        });
    }
}
