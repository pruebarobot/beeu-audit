<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directions', function (Blueprint $table) {
            $table->id();
            $table->string  ( 'street' )->nullable();
            $table->string  ( 'internal_number' )->nullable();
            $table->string  ( 'external_number' )->nullable();
            $table->string  ( 'zip_code', 5 )->nullable();
            $table->string  ( 'suburb' )->nullable();
            $table->string  ( 'city' )->nullable();
            $table->string ( 'state')->nullable();
            $table->string ( 'municipality' )->nullable();
            $table->string ( 'country' )->nullable();
            $table->longText( 'reference' )->nullable();
            $table->string ( 'phone' )->nullable();
            $table->string ( 'extension' )->nullable();
            $table->string ( 'email' )->nullable();
            $table->string ( 'contact_name' )->nullable();
            $table->string ( 'contact_job' )->nullable();
            $table->integer ( 'company_organization_id' )->nullable();
            $table->enum('type', ['Company', 'Organization']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directions');
    }
}
