<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EvaluationAuditors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluation_histories', function (Blueprint $table) {
            $table->text('qualificationfile')->nullable();
            $table->text('client_qualificationfile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluation_histories', function (Blueprint $table) {
            $table->text('qualificationfile')->nullable();
            $table->text('client_qualificationfile')->nullable();
        });
    }
}
