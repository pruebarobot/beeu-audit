<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueLastnameUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('lastname')->default('')->change();
            $table->string('secondname')->default('')->change();
            $table->string('avatar')->default('')->change();
            $table->string('cellphone')->default('')->change();
            $table->string('code')->default('')->change();
            $table->string('status')->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->string('lastname')->default(null)->change();
            $table->string('secondname')->default(null)->change();
            $table->string('avatar')->default(null)->change();
            $table->string('cellphone')->default(null)->change();
            $table->string('code')->default(null)->change();
            $table->string('status')->default(null)->change();
        });
    }
}
