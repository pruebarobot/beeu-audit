<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_histories', function (Blueprint $table) {
            $table->id();
            $table->string('company')->nullable();
            $table->date('date')->nullable();
            $table->string('type')->nullable();
            $table->string('period')->nullable();
            $table->string('qualification')->nullable();
            $table->string('evaluation')->nullable();
            $table->string('client_qualification')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_histories');
    }
}
