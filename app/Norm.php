<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Norm extends Model
{
	protected $table    = 'norms';
	
	protected $fillable = [ 'name'];
	
	protected $guarded  = [ 'id' ];
}
