<?php

namespace App\Functions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Avatar extends Model
{
    public static function setPicturePerfil($foto){
        if($foto){
            $año       =Date('Y');
            $mes       =Date('M');
            $time      = time();
            $prefix    = '_';
            $imageName = Str::random(10) . '.jpg';
            $imagen    = Image::make($foto)->encode('jpg',90);
            $imagen->resize(180,180,function($constraint){
                $constraint->upsize();
            });
            Storage::disk('public')->put("users/$año/$mes/$prefix.$imageName",$imagen->stream());
            return (object)array(
                "fullPath" =>  "users/$año/$mes/$prefix.$imageName",
                "typeFile" => ".jpg",
                "fileName" => $prefix.$imageName
            );
        }else{
            return false;
        }
    }  
    public static function setPicturePerfilCompany($foto){
        if($foto){
            $año       =Date('Y');
            $mes       =Date('M');
            $time      = time();
            $prefix    = '_';
            $imageName = Str::random(10) . '.jpg';
            $imagen    = Image::make($foto)->encode('jpg',90);
            $imagen->resize(180,180,function($constraint){
                $constraint->upsize();
            });
            Storage::disk('public')->put("company/$año/$mes/$prefix.$imageName",$imagen->stream());
            return (object)array(
                "fullPath" =>  "company/$año/$mes/$prefix.$imageName",
                "typeFile" => ".jpg",
                "fileName" => $prefix.$imageName
            );
        }else{
            return false;
        }
    }  
}
