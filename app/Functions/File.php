<?php

namespace App\Functions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends Model
{
  
    public static function saveFileCourse($document){
        if($document){
             $año          =Date('Y');
             $mes          =Date('M');
             $time         = time();
             $prefix       = '_';
             $nombre       = $document->getClientOriginalName();
             $documentName = Str::random(10).$nombre;
             Storage::disk('public')->put("courses/$año/$mes/$prefix.$documentName",\File::get($document));
        return (object)array(
            "fullPath" =>  "courses/$año/$mes/$prefix.$documentName",
            );
        }else{
            return false;
        }
    }  

}
