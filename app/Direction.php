<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $table    = 'directions';
	
	protected $fillable = ['street','internal_number','external_number','zip_code','suburb','city','state','municipality','country','reference','phone','extension','email','contact_name','contact_job','company_organization_id','type'];
	
	protected $guarded  = [ 'id' ];
}
