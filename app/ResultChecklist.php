<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultChecklist extends Model
{
    public $timestamps  = true;

	protected $table    = 'result_checklists';

	protected $fillable = ['audit_sector_norm_id','conform','observation','code','description','help','result_section_id','type','user_id'];   

	protected $guarded  = ['id'];

	public function sectionResultCheklist()
	{
		return $this->belongsTo('App\ResultSection','result_section_id','id');
	}

	public function sectorNorm()
	{
		return $this->belongsTo('App\AuditSectorNorm','audit_sector_norm_id','id');
	}
}
