<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Exception;
use App\Functions\Avatar;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public  function index(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador','Comercial'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        if($request->get('search')){
            $nombre = $request->get('search');
            $users  = User::orWhere('name','like',"%$nombre%")->orWhere('lastname','like',"%$nombre%")->orWhere('secondname','like',"%$nombre%")->orWhere('email','like',"%$nombre%")->paginate(10);
        }else{
            $users  =User::where('id','>',0)->paginate(10);;
        }

    	return view('user.index',compact('users'));
    }

    public function store(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $validate=$this->validate(request(),[
            'nombre'           =>'required',
            'apellidomaterno'  =>'required',
            'apellidopaterno'  =>'required',
            'email'            =>'required|unique:users,email',
            'password'         =>'required',
            'perfil'           =>'required',
            'telefono'         =>'required',
            'codigo'           =>'required|unique:users,code',
            'status'           =>'required',
        ]);

    	if($request->hasFile('foto')){
            $file = $request->file('foto');
            $foto=Avatar::setPicturePerfil($file);
            $img  = $foto->fullPath;
        }else{
            $img  ="users/avatar.png";
        }


        $user = User::create([
            'name'       =>$validate['nombre'],
            'lastname'   =>$validate['apellidomaterno'],
            'secondname' =>$validate['apellidopaterno'],
            'email'      =>$validate['email'],
            'password'   =>Hash::make($validate['password']),
            'avatar'     =>$img,
            'profile'    =>$validate['perfil'],
            'cellphone'  =>$validate['telefono'],
            'code'       =>$validate['codigo'],
            'status'     =>$validate['status']
        ]);

        if($user){
            return back()->with('success',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }

    }

    public function edit($id)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('delete',"No tienes permisos pata ingrear a esta opción");
        }

        $user    = User::where('id',$id)->get();

        return view('user.editusers',compact('user'));
    }

    public function update(Request $request,$id)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $updateUser = User::find($id);

        $img      = '';
        $password = '';
        if($request['password']){
            $password = Hash::make($request['password']);
        }else{
            $password = $updateUser->password;
        }

        if($request->hasFile('img')){
            $file   = $request->file('img');
            $avatar = Avatar::setPicturePerfil($file);
            $img    = $avatar->fullPath;
        }else{
            $img=$request['avatar'];
        }

        $updateUser->update([
            'name'       =>$request['name'],
            'lastname'   =>$request['lastname'],
            'secondname' =>$request['secondname'],
            'email'      =>$request['email'],
            'password'   =>$password,
            'avatar'     =>$img,
            'profile'    =>$request['profile'],
            'cellphone'  =>$request['cellphone'],
            'code'       =>$request['code'],
            'status'     =>$request['status']
        ]);

        if($updateUser){
            return redirect()->back()->with('updates',"Datos guardado");

        }
        else{
            return redirect()->back()->with('error',"Datos guardado");
        }
    }

    public function destroyd(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $user = User::find($request["user_id"]);

        try {
            $user->delete();
            return back()->with('delete',"Datos Eliminados");
        } catch (Exception $e) {
            $user->update([
                'status'=>'Inactivo'
            ]);
            return back()->with('erroruser',"Error");
        }

    }

}
