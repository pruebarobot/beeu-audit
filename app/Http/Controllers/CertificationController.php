<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\OrganizationSectorNorm;
use DB;
use App\Certification;
use App\CertificateSectorNorm;
use App\CompanySector;
use App\Sector;
use App\Norm;
use App\Audit;
use App\User;
use App\AuditTeam;
use App\AuditSectorNorm;
use App\Checklist;
use App\SectionChecklist;
use App\ResultSection;
use App\ResultChecklist;
use App\StatusChecklist;
use Exception;
use Auth;
use App\Conform;
use App\Certificate;
use App\AuditHistory;
use App\Classes\CertifactionUtils;

class CertificationController extends Controller
{
    public function index(Request $request)
    {
        $certificationUtils = new CertifactionUtils;
        $sectors            = Sector::all();
        $norms              = Norm::all();
        $organizations      = Organization::where('id', '>', 0);
        $nombre             = $request->get('search');
        $sector             = $request->get('sectors');
        $norm               = $request->get('norms');

        if($nombre){
            $organizations->Where('name','like',"%$nombre%")->orWhere('rfc','like',"%$nombre%")->orWhere('code','like',"%$nombre%");
        }

        if($sector){
            $organizations->whereHas('organizationSector',function($q) use($sector) {
                $q->whereHas('companies', function($query) use($sector) {
                    $query->Where('sector_id',$sector);
                });
            });
        }


        if($norm){
            $organizations->whereHas('organizationSector',function($q) use($norm) {
                $q->whereHas('companies', function($query) use($norm) {
                    $query->Where('norm_id',$norm);
                });
            });
        }

        $organizations = $organizations->paginate(10);
    	return view('certification.index',compact('certificationUtils','organizations','sectors','norms'));
    }

    public function changeSection(Request $request)
    {
    	$id = $request['id'];
    	$organizationsector = DB::Select("SELECT cs.id,s.name ,n.name as norma,s.id as sector_id,n.id as norm_id FROM organization_sector_norms osn, company_sectors cs ,sectors s ,norms n,organizations o where osn.company_sector_id = cs.id and cs.sector_id = s.id and cs.norm_id = n.id and osn.organization_id = o.id and o.id = $id[0]");
    	return \Response::json($organizationsector);
    }
    public function store(Request $request)
    {
    	$certification = Certification::create([
            'init_date'       => $request['date'],
			'end_date'        => $request['date_end'],
			'status'          => "Activo",
			'organization_id' => $request['idorganization'],
            'no_certificate'  => $request['no_certificate'],
    	]);

    	for ($i = 0; $i < count($request->detailsorganization); $i++) {
			$company_sectors = CompanySector::find($request['detailsorganization'][$i]);
			$sector          = Sector::find($company_sectors->sector_id);
			$norm            = Norm::find($company_sectors->norm_id);

	    	$certificationdetails = CertificateSectorNorm::create([
				'sector_id'        => $sector->id,
				'norm_id'          => $norm->id,
				'certification_id' => $certification->id,
	    	]);
        }
        if($certification){
            return back()->with('success',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
    }
    public function saveAudit(Request $request)
    {

        if($request['date_finish'] < $request['date_start']){
            return back()->with('error',"La fecha final debe ser mayor a la fecha inicial");
        }

        DB::beginTransaction();

        try{
            $sections         = [];
            $resultChecklists = [];
            $resultSection    = [];
            $resultSections   = [];
            $validationNorm   = [];

            $audit = Audit::create([
                'type'             => $request['type'],
                'status'           => "En progreso",
                'certification_id' => $request['id'],
                'init_date'        => $request['date_start'],
                'finish_date'      => $request['date_finish'],
            ]);

            for ($i = 0; $i < count($request->sectores); $i++) {
                $resultChecklists = [];

                $auditSectoNorm = AuditSectorNorm::create([
                    'sector_id'    => $request['sectores'][$i],
                    'norm_id'      => $request['norms'][$i],
                    'audit_id'     => $audit->id,
                ]);
                if (!in_array($request['norms'][$i], $validationNorm)) {
                   array_push($validationNorm,$request['norms'][$i]);

                    $companySector = CompanySector::where('sector_id',$auditSectoNorm->sector_id)->where('norm_id',$auditSectoNorm->norm_id)->first();

                    $checklists = Checklist::where('company_sector_id',$companySector->id)->where('audit_type',$request['type'])->get();

                    if(count($checklists)==0){
                        $checklists = Checklist::where('company_sector_id',$companySector->id)->get();
                    }

                    foreach ($checklists as $checklist) {
                        $sections[$checklist->section_checklist_id] = ["cheklist"=>$checklist];

                        $resultChecklist = ResultChecklist::create([
                            'audit_sector_norm_id'        => $auditSectoNorm->id,
                            'conform'                     => '',
                            'observation'                 => '',
                            'code'                        => $checklist->code,
                            'description'                 => $checklist->description,
                            'type'                        => $checklist->type,
                            'help'                        => $checklist->help,
                        ]);

                        $resultChecklists[] = ["resultchecklist" => $resultChecklist,"sectionCheklistId"=>$checklist->section_checklist_id];
                    }

                    foreach ($sections as $section) {
                        $checklist       = $section["cheklist"];
                        $checklistSector = $checklist->companySector->sector_id;
                        $checklistNorm   = $checklist->companySector->norm_id;

                        if($checklistSector == $auditSectoNorm->sector_id && $checklistNorm == $auditSectoNorm->norm_id){
                            $resultSection = ResultSection::create([
                                'audit_sector_norm_id' => $auditSectoNorm->id,
                                'observation'          => '',
                                'code'                 => $checklist->sectionCheklist->code,
                                'description'          => $checklist->sectionCheklist->description,
                                'audit_id'             => $audit->id,
                            ]);

                        }
                        
                        $resultSections[$checklist->section_checklist_id] = $resultSection->id;

                    }


                    $checklistStatus = StatusChecklist::create([
                        'audit_sector_norm_id' => $auditSectoNorm->id,
                        'audit_id'             => $audit->id,
                        'status'               => "Activo",
                    ]);

                    foreach ($resultChecklists as $resultChecklist) {
                        $resultChecklist["resultchecklist"]->update([
                            'result_section_id' => $resultSections[$resultChecklist["sectionCheklistId"]],
                        ]);
                    }
                }
            }

            DB::commit();

            return back()->with('success',"Dato guardado");

        }catch (Exception $e) {

            DB::rollBack();
            return back()->with('error',"Ocurrio un error");
        }
    }


    public function saveTeam(Request $request)
    {
        $team = AuditTeam::create([
            'user_id'  =>  $request['auditor'],
            'audit_id' =>  $request['id'],
            'function' =>  $request['funcion'],
            'date'     =>  $request['fecha'],
            'date_end' =>  $request['fecha_end'],
            'hour'     =>  $request['horas'],
            'minute'   =>  $request['minutos'],
        ]);

        if($team){
            return back()->with('success',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
    }
    public function deleteTeam(Request $request)
    {
        $team = AuditTeam::find($request['id']);
        $team->delete();

        if($team){
            return back()->with('success',"Dato eliminado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }

    }

    public function progress($id)
    {
        $audits          = Audit::where('certification_id',$id)->orderBy('created_at','ASC')->get();
        $auditors        = User::where('profile','Auditor')->get();
        $certifications  = Certification::where('id',$id)->take(1)->get();
        $organizacion_id = $certifications;
        $auditeam        = AuditTeam::all();
        $sectornorms     = CertificateSectorNorm::where('certification_id',$id)->get();
        $sectionlists    = SectionChecklist::all();
        $resultconsult   = [];
        $checklistdata   = [];
        $companysectors  = [];

        for ($i=0; $i < count($sectornorms); $i++) {
            $sector     = $sectornorms[$i]->sector_id;
            $norm       = $sectornorms[$i]->norm_id;
            $result     = CompanySector::where('sector_id',$sector)->where('norm_id',$norm)->take(1)->get();
            foreach ($result as $rest) {
            array_push($resultconsult,$rest->id);
            array_push($companysectors,$rest);
            }
        }
        for ($i=0; $i < count($resultconsult); $i++) {
            $checklists = Checklist::where('company_sector_id',$resultconsult[$i])->get();
            foreach ($checklists as $checklist) {
                array_push($checklistdata,$checklist);
            }
        }
        $organizations   = Organization::find($organizacion_id[0]->organization_id);
        return view('certification.progress',compact('audits','auditors','auditeam','organizations','checklistdata','companysectors','sectionlists'));
    }

    public function auditDetails($id)
    {
        $auditors         = User::where('profile','Auditor')->get();
        $auditeam         = AuditTeam::where('audit_id',$id)->get();
        $audits           = Audit::find($id);
        $audit            = Audit::find($id);
        $auditscomplete   = $audits->certification->audit;
        $organizations    = $audits->certification->certification;
        $validationUser   = AuditTeam::where('audit_id',$id)->where('user_id', Auth::user()->id)->first();

        $filterNorm     = [];
        $filterUserNorm = [];
        $filterSector = [];

        $normasAuditoria = $audits->auditsector;
        $normasUser      = Auth::user()->AuditorSector;

        foreach ($normasUser as $userNorm) {
            $filterUserNorm[$userNorm->company->norm_id] = $userNorm->company->norm->name;
        }

        foreach ($normasAuditoria as $sectorNorm) {
            $filterNorm[$sectorNorm->norm_id] = $sectorNorm->norm->name;
            $filterSector[$sectorNorm->sector_id] = $sectorNorm->sector->name;
        }

        // dump($filterNorm);

        return view('certification.audit',compact('audits','audit','auditors','auditeam','organizations','auditscomplete','validationUser','filterNorm','filterUserNorm','filterSector','normasAuditoria'));
    }

    public function endAndStarAuditori(Request $request)
    {
        $auditOld = Audit::find($request['auditId']);
        $validationNorm   = [];

        foreach ($auditOld->auditsector as $sectorNorm) {
            foreach($auditOld->teams as $team){
                $auditor = AuditHistory::create([
                    'name_company' => $auditOld->certification->certification->name,
                    'init_date'    => $auditOld->init_date,
                    'finish_date'  => $auditOld->finish_date,
                    'sector'       => $sectorNorm->sector->name,
                    'norm'         => $sectorNorm->norm->name,
                    'roll'         => $team->function,
                    'user_id'      => $team->user_id
                ]);
            }
        }

        $auditOld->update([
                'status'   => "Terminado",
                'finished' => 1,
        ]);
        DB::beginTransaction();
        try{
            $audit = Audit::create([
                'type'             => $request['type'],
                'status'           => "En progreso",
                'certification_id' => $auditOld->certification_id,
                'init_date'        => $request['date_start'],
                'finish_date'      => $request['date_finish'],
            ]);

            for ($i = 0; $i < count($request->sectores); $i++) {
                $resultChecklists = [];
                $auditSectoNorm = AuditSectorNorm::create([
                    'sector_id'    => $request['sectores'][$i],
                    'norm_id'      => $request['norms'][$i],
                    'audit_id'     => $audit->id,
                ]);
                if (!in_array($request['norms'][$i], $validationNorm)) {
                    array_push($validationNorm,$request['norms'][$i]);


                    $companySector = CompanySector::where('sector_id',$auditSectoNorm->sector_id)->where('norm_id',$auditSectoNorm->norm_id)->first();


                    $checklists = Checklist::where('company_sector_id',$companySector->id)->where('audit_type',$request['type'])->get();

                    if(count($checklists)==0){
                        $checklists = Checklist::where('company_sector_id',$companySector->id)->get();
                    }

                    foreach ($checklists as $checklist) {
                        $sections[$checklist->section_checklist_id] = ["cheklist"=>$checklist];

                        $resultChecklist = ResultChecklist::create([
                            'audit_sector_norm_id'        => $auditSectoNorm->id,
                            'conform'                     => '',
                            'observation'                 => '',
                            'type'                        => $checklist->type,
                            'help'                        => $checklist->help,
                            'code'                        => $checklist->code,
                            'description'                 => $checklist->description,
                            'user_id'                     => Auth::user()->id,  
                        ]);

                        $resultChecklists[] = ["resultchecklist" => $resultChecklist,"sectionCheklistId"=>$checklist->section_checklist_id];
                    }

                    foreach ($sections as $section) {
                        $checklist       = $section["cheklist"];
                        $checklistSector = $checklist->companySector->sector_id;
                        $checklistNorm   = $checklist->companySector->norm_id;

                        if($checklistSector == $auditSectoNorm->sector_id && $checklistNorm == $auditSectoNorm->norm_id){
                            $resultSection = ResultSection::create([
                                'audit_sector_norm_id' => $auditSectoNorm->id,
                                'observation'          => '',
                                'code'                 => $checklist->sectionCheklist->code,
                                'description'          => $checklist->sectionCheklist->description,
                                'audit_id'             => $audit->id,
                            ]);
                        }
                        $resultSections[$checklist->section_checklist_id] = $resultSection->id;
                    }
                    
                    $checklistStatus = StatusChecklist::create([
                        'audit_sector_norm_id' => $auditSectoNorm->id,
                        'audit_id'             => $audit->id,
                        'status'               => "Activo",
                    ]);
                    

                    foreach ($resultChecklists as $resultChecklist) {
                        $resultChecklist["resultchecklist"]->update([
                            'result_section_id' => $resultSections[$resultChecklist["sectionCheklistId"]],
                        ]);
                    }
                }
            }

            DB::commit();

            return redirect()->route('progress', ['id' => $auditOld->certification_id]);

        }catch (Exception $e) {

            DB::rollBack();
            return back()->with('error',"Ocurrio un error");
        }

    }
    public function delete(Request $request)
    {
        $certification=Certification::find($request['id']);
        $certification->update([
            'status' => 'Cancelado'
        ]);
        return back()->with('delete',"Dato eliminado");
    }

    public function endAudit(Request $request)
    {
        $auditOld = Audit::find($request['auditId']);

        foreach ($auditOld->auditsector as $sectorNorm) {
            foreach($auditOld->teams as $team){
                $auditor = AuditHistory::create([
                    'name_company' => $auditOld->certification->certification->name,
                    'init_date'    => $auditOld->init_date,
                    'finish_date'  => $auditOld->finish_date,
                    'sector'       => $sectorNorm->sector->name,
                    'norm'         => $sectorNorm->norm->name,
                    'roll'         => $team->function,
                    'user_id'      => $team->user_id
                ]);
            }
        }

        $auditOld->update([
                'status'   => "Terminado",
                'finished' => 1,
        ]);
        // $certification=Certification::find($request['auditId']);
        // $certification->update([
        //     'status' => 'Finalizado'
        // ]);
        return \Redirect::route('certification')->with('auditend', 'OK');

    }

    public function generateFile(Request $request)
    {   
        // return $request;
        // $checklist = ResultChecklist::find(1185);
        // $normaId   = $checklist->sectorNorm->norm_id;
        // $sectorId  = $checklist->sectorNorm->sector_id;
        // $auditId   = $checklist->sectionResultCheklist->audit_id;
         
        // $auditSectorNorm = AuditSectorNorm::where('norm_id',$normaId)->where('audit_id',$auditId)->where('sector_id','!=',$sectorId)->get();

        // foreach ($auditSectorNorm as $sectorNorm) {
        //     $clonChecklist   = ResultChecklist::where('audit_sector_norm_id',$sectorNorm->id)->where('code',$checklist->code)->where('description',$checklist->description)->get();
        // }

        // $resultCheck = ResultSection::where('audit_id',$auditId)->get();

        // for ($i=0; $i <count($resultCheck) ; $i++) { 
        //     if($norma == $resultCheck[$i]->sectorNorm->norm_id){
        //         dump($resultCheck[$i]->sectionResultCheklist);

        //     }
        //  }

        $sectionId = [];
        if($request['id']){
            for ($i=0; $i < count($request['id']) ; $i++) { 
                $checklist = ResultChecklist::find($request['id'][$i]);
                if($request['observationCheck'][$i]){

                    if($request['aplicacheck'][$i]=="NA"){
                        if(!$checklist->observation){
                            $checklist->update([
                                'conform'      => "NA",
                                'observation'  => $request['observationCheck'][$i],
                            ]);

                        }
                    }else{
                        if(!$checklist->conform){
                            if($request['conformcheck'][$i]=="NC"){
                                $checklist->update([
                                    'conform'      => $request['conformcheck'][$i],
                                    'observation'  => $request['observationCheck'][$i],
                                ]);
                            }else{
                                $checklist->update([
                                    'conform'      => $request['conformcheck'][$i],
                                    'observation'  => $request['observationCheck'][$i],
                                ]);
                            }
                        }
                    }
                    $checklist->update([
                                'user_id' => Auth::user()->id,
                                
                    ]);
                }

            }
        }

        for ($i=0; $i < count($request['idSection']) ; $i++) { 
            $checklist = ResultChecklist::find($request['idSection'][$i]);
            array_push($sectionId,$checklist->result_section_id);
        }


        $sectionId = array_values(array_unique($sectionId));

        for ($i=0; $i < count($sectionId) ; $i++) { 
            $resultSection = ResultSection::find($sectionId[$i]);
            $resultSection->update([
                'observation' => $request['observationSection'][$i]
            ]);
        }
        return "ok";
    }

    public function endChecklist(Request $request)
    {
        $validation = 0;
        for ($i=0; $i < count($request['id']) ; $i++) {
            $checklist = ResultChecklist::find($request['id'][$i]);
                if(isset($checklist->observation)){
                    echo $checklist->observation;
                }else{
                    $validation = 1;
                }

        }
        if($validation <= 1){

            for ($i=0; $i < count($request['id']) ; $i++) {
                $checklist = ResultChecklist::find($request['id'][$i]);
                $status    = StatusChecklist::Where('audit_sector_norm_id','=',$checklist->audit_sector_norm_id)->where('audit_id','=',$checklist->sectionResultCheklist->audit_id);
                $status->update([
                    'status'  => "Cerrado",
                    'user_id' => Auth::user()->id,

                ]);
            }
            return "ok";

        }else{
            return "no listo";
        }

    }

    public function exportFile(Request $request)
    {    
        try{
            $audits           = Audit::find($request['id']);
            $status           = StatusChecklist::where('audit_id',$request['id'])->where('audit_sector_norm_id',$request['sector'])->get();
            $auditeam         = AuditTeam::where('audit_id',$request['id']);
            $idvalidation     = $request['sector'];
            $resultadosection = ResultSection::where('audit_id',$request['id'])->where('audit_sector_norm_id',$request['sector'])->get();
            $countResultMayorNconform = [];
            $countResultMenorNconform = [];
            for ($i=0; $i <count($resultadosection) ; $i++) { 
                $mayorNconform   = ResultChecklist::where('result_section_id',$resultadosection[$i]->id)->where('type','Mayor')->where('conform','NC')->get()->first();
                $menorNconform   = ResultChecklist::where('result_section_id',$resultadosection[$i]->id)->where('type','Menor')->where('conform','NC')->get()->first();
                if($mayorNconform){
                    array_push($countResultMayorNconform, $mayorNconform);
                }
                if($menorNconform){
                    array_push($countResultMenorNconform, $menorNconform);
                }

            }

            if(count($countResultMayorNconform)>=1){
                $resultado = "No";
            }else if(count($countResultMenorNconform)>=5){
                $resultado = "No";
            }else{
                $resultado = "Si";
            }

            $pdf = \PDF::loadView('pdf.certification',compact('audits','auditeam','status','resultadosection','idvalidation','resultado'));
            return $pdf->stream('Certificado.pdf');

        }catch (Exception $e) {

            return $e;
        }
    }


    public function saveNoConform(Request $request)
    {

        if($request['exclusiones']){
            $status = StatusChecklist::find($request['id']);
            $status->update([
                'exclusions'       => $request['exclusiones'],
                'code'             => $request['code'],
                'actionNoConforms' => $request['actionNoConforms'],
                'alcance'          => $request['alcance'],
            ]);
        }

        if($request['description']){
           $confomr = Conform::create([
                'code'           => $request['requisitos'],
                'description'    => $request['description'],
                'type_nc'        => $request['nc'],
                'statuscheck_id' => $request['id'],
           ]);
        }

            return back()->with('success',"Dato guardado");

    }
    public function saveCertificateFile(Request $request)
    {
        if($request['certificatePath']){
            $cetificate = Certificate::find($request['certificatePath']);
            $cetificate->update([
                'file_path' => $request->file('certificadopdf')->store('public/certificates'),
            ]);
        }else{
            $certificate = Certificate::create([
                'audit_id'  =>$request['id'],
                'file_path' =>$request->file('certificadopdf')->store('public/certificates'),
                'init_date' =>$request['datestart'],
                'end_date'  =>$request['dateend'],
                'type'      =>$request['type'],
                'status'    =>'Valido',
            ]);
        }

        return back()->with('success',"Dato guardado");

    }

    public function editCertification(Request $request)
    {
        $certification = Certification::find($request['id']);
        $certification->update([
            'init_date'  =>  $request['init_date']
        ]);

        return back()->with('success',"Dato guardado");

    }

    public function certificates(Request $request)
    {
        $certificates = Certificate::where('id','>',0);
        $search       = $request['search'];

        if($request['type']){
            $certificates->where('type',$request['type']);
        }

        if($request['search']){
            $certificates->whereHas('Audit',function($q) use($search) {
                $q->whereHas('certification', function($query) use($search) {
                   $query->whereHas('certification', function($querys) use($search) {
                        $querys->orWhere('name','like',"%$search%")->orWhere('code','like',"%$search%");;
                    });
                });
            });
        }

        $certificates = $certificates->paginate(10);
        return view('certification.certificates',compact('certificates'));
    }

    public function saveFileExpedient(Request $request)
    {
        $audit = Audit::find($request['id']);
        if ($request->hasFile('expediente')) {
        $tamano = $request->file('expediente')->getSize();
            if($tamano <= 12118260){
                $audit->update([
                    'expedient' =>$request->file('expediente')->store('public/expediente'),
                ]);
            }else{
                return back()->with('error',"Archivo demasiado pesado el maximo es de 12mb");
            }
        }
        return back()->with('success',"Dato guardado");
    }

    public function endExpedient(Request $request)
    {
        $audit = Audit::find($request['id']);
        $audit->update([
            'end_expedient' => 1,
        ]);
        
        return back()->with('success',"Dato guardado");
    }


    public function result(Request $request)
    {
        $id         = $request['id'];
        $sectornorm = $request['sector'];
        $resultadosection = ResultSection::where('audit_id',$id)->where('audit_sector_norm_id',$sectornorm)->get();

        $countResultMayorNconform = [];
        $countResultMenorNconform = [];

        for ($i=0; $i <count($resultadosection) ; $i++) { 
            $mayorNconform   = ResultChecklist::where('result_section_id',$resultadosection[$i]->id)->where('type','Mayor')->where('conform','NC')->get()->first();
            $menorNconform   = ResultChecklist::where('result_section_id',$resultadosection[$i]->id)->where('type','Menor')->where('conform','NC')->get()->first();
            if($mayorNconform){
                array_push($countResultMayorNconform, $mayorNconform);
            }
            if($menorNconform){
                array_push($countResultMenorNconform, $menorNconform);
            }
        }

        if(count($countResultMayorNconform)>=1){
            $resultado = "No";
        }else if(count($countResultMenorNconform)>=5){
            $resultado = "No";
        }else{
            $resultado = "Si";
        }

        return \Response::json($resultado);

    }

    public function editCertificationfile(Request $request)
    {
        $cetificate = Certificate::find($request['certificatePath']);
        if($request['certificadopdf']){
            $cetificate->update([
                'file_path' => $request->file('certificadopdf')->store('public/certificates'),
            ]);
        }

        $cetificate->update([
            'status'    =>$request['status'],
        ]);

        return back()->with('success',"Datos guardados");

    }

    public function changeStatus(Request $request)
    {
        $cetificate = Certification::find($request['id']);
        $estatus = '';
        switch ($request['statusChange']) {
            case 'Activo':
                $estatus =  'Valido';
                break;
            case 'Finalizado':
                $estatus =  'Suspendido';
                break;
            case 'Cancelado':
                $estatus =  'Cancelado';
                break;
        }

        foreach($cetificate->audit as $audit){
            foreach($audit->certificates as $certificates){
            $certificates->update([
                 'status' => $estatus
                ]);
            }
        }
        $cetificate->update([
            'status' => $request['statusChange'],
        ]);
        return back()->with('success',"Datos guardados");

    }


    public function saveauditrestart(Request $request)
    {

        if($request['date_finish'] < $request['date_start']){
            return back()->with('error',"La fecha final debe ser mayor a la fecha inicial");
        }

        DB::beginTransaction();

        try{

            $auditOld = Audit::find($request['idAudit']);
            
            foreach ($auditOld->auditsector as $sectorNorm) {
                foreach($auditOld->teams as $team){
                    $auditor = AuditHistory::create([
                        'name_company' => $auditOld->certification->certification->name,
                        'init_date'    => $auditOld->init_date,
                        'finish_date'  => $auditOld->finish_date,
                        'sector'       => $sectorNorm->sector->name,
                        'norm'         => $sectorNorm->norm->name,
                        'roll'         => $team->function,
                        'user_id'      => $team->user_id
                        ]);
                    }
                }
                
            $auditOld->update([
                'status'   => "Terminado",
                'finished' => 1,
            ]);
            
            $sections         = [];
            $resultChecklists = [];
            $resultSection    = [];
            $resultSections   = [];
            $validationNorm   = [];

            $audit = Audit::create([
                'type'             => $request['type'],
                'status'           => "En progreso",
                'certification_id' => $request['id'],
                'init_date'        => $request['date_start'],
                'finish_date'      => $request['date_finish'],
            ]);

            for ($i = 0; $i < count($request->sectores); $i++) {
                $resultChecklists = [];

                $auditSectoNorm = AuditSectorNorm::create([
                    'sector_id'    => $request['sectores'][$i],
                    'norm_id'      => $request['norms'][$i],
                    'audit_id'     => $audit->id,
                ]);
                if (!in_array($request['norms'][$i], $validationNorm)) {
                   array_push($validationNorm,$request['norms'][$i]);

                    $companySector = CompanySector::where('sector_id',$auditSectoNorm->sector_id)->where('norm_id',$auditSectoNorm->norm_id)->first();

                    $checklists = Checklist::where('company_sector_id',$companySector->id)->where('audit_type',$request['type'])->get();

                    if(count($checklists)==0){
                        $checklists = Checklist::where('company_sector_id',$companySector->id)->get();
                    }

                    foreach ($checklists as $checklist) {
                        $sections[$checklist->section_checklist_id] = ["cheklist"=>$checklist];

                        $resultChecklist = ResultChecklist::create([
                            'audit_sector_norm_id'        => $auditSectoNorm->id,
                            'conform'                     => '',
                            'observation'                 => '',
                            'code'                        => $checklist->code,
                            'description'                 => $checklist->description,
                            'type'                        => $checklist->type,
                            'help'                        => $checklist->help,
                        ]);

                        $resultChecklists[] = ["resultchecklist" => $resultChecklist,"sectionCheklistId"=>$checklist->section_checklist_id];
                    }

                    foreach ($sections as $section) {
                        $checklist       = $section["cheklist"];
                        $checklistSector = $checklist->companySector->sector_id;
                        $checklistNorm   = $checklist->companySector->norm_id;

                        if($checklistSector == $auditSectoNorm->sector_id && $checklistNorm == $auditSectoNorm->norm_id){
                            $resultSection = ResultSection::create([
                                'audit_sector_norm_id' => $auditSectoNorm->id,
                                'observation'          => '',
                                'code'                 => $checklist->sectionCheklist->code,
                                'description'          => $checklist->sectionCheklist->description,
                                'audit_id'             => $audit->id,
                            ]);

                        }
                        
                        $resultSections[$checklist->section_checklist_id] = $resultSection->id;

                    }


                    $checklistStatus = StatusChecklist::create([
                        'audit_sector_norm_id' => $auditSectoNorm->id,
                        'audit_id'             => $audit->id,
                        'status'               => "Activo",
                    ]);

                    foreach ($resultChecklists as $resultChecklist) {
                        $resultChecklist["resultchecklist"]->update([
                            'result_section_id' => $resultSections[$resultChecklist["sectionCheklistId"]],
                        ]);
                    }
                }
            }

            

            DB::commit();
                    
            return back()->with('success',"Dato guardado");

        }catch (Exception $e) {

            DB::rollBack();
            return back()->with('error',"Ocurrio un error");
        }
    }
}
