<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Functions\Avatar;
use App\Sector;
use App\Norm;
use App\Direction;
use App\CompanySector;
use Exception;
use App\Classes\Selection;


class CompanyController extends Controller
{
    public function index()
    {
		$companies      =Company::all();
		$sectors        =Sector::all();
		$norms          =Norm::all();
		$companysectors =CompanySector::all();
    	return view('company.index',compact('companies','sectors','norms','companysectors'));
    }
    public function store(Request $request)
    {
    	$normId = "";
    	try { 
    		$validateError = false;
    		for ($i = 0; $i < count($request->sectors); $i++) {
	            for ($i = 0; $i < count($request->norms); $i++) {
	            	$validation = CompanySector::where('sector_id',$request['sectors'][$i])->Where('norm_id',$request['norms'][$i])->get();
	            	if(count($validation)<=0){
		                $companysector=CompanySector::create([
								'sector_id'    => $request['sectors'][$i],
								'norm_id'      => $request['norms'][$i],
								'companies_id' => 1,
		                ]);

		            	$norm = Norm::find($request['norms'][$i])->first();
		            	if($norm->name=="ISO 9001:2015"){
		            		$normId = '9';
		            	}elseif($norm->name=="ISO 14001:2015"){
		            		$normId = '14';
		            	}elseif($norm->name=="ISO 22000:2005"){
		            		$normId = '22';
		            	}elseif($norm->name=="ISO 45001:2018"){
		            		$normId = '45';
		            	}

	                	$checklist = Selection::check($companysector->id,$normId);

	            	}else{
	            		$validateError = true;
	            	}
	            }
        	}

        	
  			return back()->with('success',"Dato guardado");

		} catch (Exception $e) { 
	        return back()->with('error',"Ocurrio un error");
		}

    }
    public function update(Request $request,$id)
    {
		if($request->hasFile('img')){
			$file   = $request->file('img');
			$foto   = Avatar::setPicturePerfilCompany($file);  
			$avatar = $foto->fullPath;
        }else{
        	$avatar = $request['avatar'];
        }

	    if($company = Company::find($id)){
	        $company->update([
				'name'              =>$request['name'],
				'website'           =>$request['website'],
				'acreditation_body' =>$request['acreditationBody'],
				'rfc'             	=>$request['rfc'],
				'avatar'            =>$avatar
	        ]);
	        $direction = Direction::find($request['direction_id']);

	        $direction->update([
				'street'                  =>$request['steet'],
				'internal_number'         =>$request['numberInt'],
				'external_number'         =>$request['numberOut'],
				'zip_code'                =>$request['cp'],
				'suburb'                  =>$request['suburb'],
				'city'                    =>$request['city'],
				'state'                   =>$request['state'],
				'municipality'            =>$request['municipality'],
				'country'                 =>$request['country'],
				'reference'               =>$request['reference'],
				'phone'                   =>$request['phone'],
				'extension'               =>$request['extension'],
				'email'                   =>$request['email'],
				'contact_name'            =>$request['contact_name'],
				'contact_job'             =>$request['contact_job'],

	        ]);            
	    
	        return back()->with('success',"Dato guardado");
	    }
	    else{
	        return back()->with('error',"Ocurrio un error");
	    }
    }

    public function editSector(Request $request)
    {
        $companysector=CompanySector::find($request['id']);

        $companysector->update([
			'sector_id'    => $request['sectors'],
			'norm_id'      => $request['norms'],
        ]);

        if($companysector){
        	return back()->with('success',"Dato guardado");
	    }
	    else{
	        return back()->with('error',"Ocurrio un error");
	    }
    }
    public function deleteSector(Request $request)
    {
        $companysector=CompanySector::find($request['id']);
        
        try { 
        	$companysector->delete();
        	return back()->with('delete',"Dato eliminado");

		} catch (Exception $e) { 
	        return back()->with('sectornorm',"El registro esta siendo utilizado en el sistema");
		 
		}

    }
}
