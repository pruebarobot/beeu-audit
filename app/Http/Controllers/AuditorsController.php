<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Sector;
use App\Norm;
use App\AuditorSector;
use Illuminate\Support\Facades\Hash;
use App\Functions\Avatar;
use App\HistoryAcademic;
use App\HistoryEmployment;
use App\Functions\File;
use App\CompanySector;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\AuditHistory;
use App\JobHistory;
use App\YearceHistory;
use App\ExperienceHistory;
use App\EvaluationHistory;

class AuditorsController extends Controller
{
    public function index(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $sectors        = Sector::all();
        $norms          = Norm::all();
        $companysectors = CompanySector::all();
        $nombre         = $request->get('search');
        $sectorInput    = $request->get('sector');
        $normaInput     = $request->get('norms');
        $users          = User::where('profile','Auditor');

        if($nombre){
            $users->where('name','like',"%$nombre%")->orWhere('lastname','like',"%$nombre%")->orWhere('secondname','like',"%$nombre%")->orWhere('email','like',"%$nombre%");
        }

        if($sectorInput){
            $users->whereHas('AuditorSector',function($q) use($sectorInput) {
                $q->whereHas('company', function($query) use($sectorInput) {
                    $query->Where('sector_id',$sectorInput);
                });
            });
        }

        if($normaInput){
            $users->whereHas('AuditorSector',function($q) use($normaInput) {
                $q->whereHas('company', function($query) use($normaInput) {
                    $query->Where('norm_id',$normaInput);
                });
            });
        }


        $users = $users->paginate(10);

        return view('auditor.index',compact('users','sectors','norms','companysectors'));
    }

    public function store(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $validate=$this->validate(request(),[
            'nombre'           =>'required',
            'apellidomaterno'  =>'required',
            'apellidopaterno'  =>'required',
            'email'            =>'required|unique:users,email',
            'password'         =>'required',
            'perfil'           =>'required',
            'telefono'         =>'required',
            'codigo'           =>'required|unique:users,code',
            'status'           =>'required',
            'norma'            =>'required',
            'sector'           =>'required',
        ]);


        if($request->hasFile('foto')){
            $file = $request->file('foto');
            $foto = Avatar::setPicturePerfil($file);
            $img  = $foto->fullPath;
        }else{
            $img  ="users/avatar.png";
        }

        $user = User::create([
            'name'       =>$request['nombre'],
            'lastname'   =>$request['apellidopaterno'],
            'secondname' =>$request['apellidomaterno'],
            'email'      =>$request['email'],
            'password'   =>Hash::make($request['password']),
            'avatar'     =>$img,
            'profile'    =>$request['perfil'],
            'cellphone'  =>$request['telefono'],
            'code'       =>$request['codigo'],
            'status'     =>$request['status']]);

        $iduser=$user->id;

        for ($i=0; $i < count($request['norma']) ; $i++) { 
            for ($x=0; $x < count( $request['sector']); $x++) { 
                $companysector = CompanySector::where('sector_id',$request['sector'][$x])->where('norm_id',$request['norma'][$i])->first();

                if($companysector){
                    $normandsectors=AuditorSector::create([
                        'company_sector_id' => $companysector->id,
                        'user_id'           => $iduser,
                    ]);
                }else{
                    $companysector = CompanySector::create([
                        'sector_id'     =>$request['sector'][$x],
                        'norm_id'       =>$request['norma'][$i],
                        'companies_id'  =>1
                    ]);

                    $normandsectors=AuditorSector::create([
                        'company_sector_id' => $companysector->id,
                        'user_id'           => $iduser,
                    ]);
                }

            }
        }


        if(isset($request->namecourse)){
            for ($i = 0; $i < count($request->namecourse); $i++) {

                if(isset($request['filecourse'][$i])){
                    $document=$request['filecourse'][$i];
                }else{
                    $document="Sin Certificado";
                }

                $academic=HistoryAcademic::create([
                    'institution'               => $request['namecourse'][$i],
                    'date'                      => $request['datecourse'][$i],
                    'carrier'                   => $request['passcourse'][$i],
                    'score'                     => $request['scorecourse'][$i],
                    'certification_institution' => $request['titlecertificate'][$i],
                    'file_path'                 => $document,
                    'user_id'                   => $iduser
                ]);
            }
        }

            $job=HistoryEmployment::create([
                'job'                 => $request['namejob'],
                'year_experience'     => $request['yearsjob'],
                'industry_experience' => $request['experiencejob'],
                'user_id'             => $iduser

            ]);

        if(isset($request->name_organizationhc)){
            for ($i=0; $i < count($request->name_organizationhc); $i++) { 
                $historial = AuditHistory::create([
                    'name_company' =>$request['name_organizationhc'][$i],
                    'init_date'    =>$request['date_firsthc'][$i],
                    'finish_date'  =>$request['date_endhc'][$i],
                    'sector'       =>$request['sectorhc'][$i],
                    'norm'         =>$request['normahc'][$i],
                    'roll'         =>$request['rolhc'][$i],
                    'user_id'      => $iduser
                ]);
            }
        }
        if(isset($request->Periodojobhistory)){
            for ($i=0; $i < count($request->Periodojobhistory); $i++) { 
                $jobhistory = JobHistory::create([
                    'period'             =>$request['Periodojobhistory'][$i],
                    'company'            =>$request['Empresajobhistory'][$i],
                    'position'           =>$request['Puestojobhistory'][$i],
                    'developed_activity' =>$request['Actividadjobhistory'][$i],
                    'user_id'            =>$iduser
                ]);
            }
        }

        if(isset($request->Empresaauditrealizated)){
            for ($i=0; $i < count($request->Empresaauditrealizated); $i++) { 
                $jobhistory = YearceHistory::create([
                    'company' =>$request['Empresaauditrealizated'][$i],
                    'type'    =>$request['Tipoauditrealizated'][$i],
                    'norm'    =>$request['Normaauditrealizated'][$i],
                    'date'    =>$request['Fechaauditrealizated'][$i],
                    'days'    =>$request['Diasauditrealizated'][$i],
                    'sector'  =>$request['Sectorauditrealizated'][$i],
                    'user_id' =>$iduser
                ]);
            }
        }

        if(isset($request->Nombreformation)){
            for ($i=0; $i < count($request->Nombreformation); $i++) { 
                $experience = ExperienceHistory::create([
                    'course'   =>$request['Nombreformation'][$i],
                    'hours'    =>$request['Horasformation'][$i],
                    'date'     =>$request['Fechaformation'][$i],
                    'days'     =>$request['Díasformation'][$i],
                    'imparted' =>$request['Impartidoformation'][$i],
                    'user_id'  => $iduser
                ]);
            }
        }


        if(isset($request->Empresaevaluation)){
            for ($i=0; $i < count($request->Empresaevaluation); $i++) { 
                $evaluation = EvaluationHistory::create([
                    'company'                  =>$request['Empresaevaluation'][$i],
                    'date'                     =>$request['Fechaevaluation'][$i],
                    'type'                     =>$request['Tipoevaluation'][$i],
                    'period'                   =>$request['Periodoevaluation'][$i],
                    'qualification'            =>$request['Calificacionevaluation'][$i],
                    'qualificationfile'        =>$request['archivoCalificacion'][$i],
                    'evaluation'               =>$request['Testificaciónevaluation'][$i],
                    'client_qualification'     =>$request['CalificacioClientenevaluation'][$i],
                    'client_qualificationfile' =>$request['archivoCalificacionCliente'][$i],
                    'user_id'                  => $iduser
                ]);
            }
        }


        if($job){
            return back()->with('success',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }

    }
    public function saveFilesEvaluation(Request $request)
    {
        $doc = $request->file('formdata')->store('public/evaluciones');

        return $doc;
    }

    public function saveFilesEvaluationCliente(Request $request)
    {
        $doc = $request->file('formdata')->store('public/evaluciones');

        return $doc;
    }


    public function edit($id)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $user           = User::where('id',$id)->get();
        $academics      = HistoryAcademic::where('user_id',$id)->get();
        $job            = HistoryEmployment::where('user_id',$id)->get();
        $sectors        = Sector::all();
        $norms          = Norm::all();
        $companysectors = CompanySector::all();
        $auditorsectors = AuditorSector::where('user_id',$id)->get();

        $sectores       = [];
        $normas         = [];
        $buscadorsector = [];
        $buscadornorma  = [];
        $sectorIds      = [];
        $normIds        = [];
        foreach ($auditorsectors as $auditorsector) {
            if($auditorsector->company->sector_id && $auditorsector->company->norm_id){
                $sectores[$auditorsector->company->sector_id]=$auditorsector->company->sector_id;
                $normas[$auditorsector->company->norm_id]=$auditorsector->company->norm_id;
            }
        }

        foreach ($sectores as $sector) {
            $buscadorsector[$sector] = Sector::find($sector);
            $sectorIds[]=$buscadorsector[$sector]->id;
        }

        foreach ($normas as $norma) {
           $buscadornorma[$norma] =  Norm::find($norma);
           $normIds[]=$buscadornorma[$norma]->id;
        }

        return view('auditor.edit',compact('user','academics','job','sectors','norms','auditorsectors','companysectors','buscadorsector','buscadornorma','sectorIds','normIds'));
    }

    public function jobUpdate(Request $request, $id)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $jobs = HistoryEmployment::find($id);

        $jobs->update([
            'job'                 => $request['namejob'],
            'year_experience'     => $request['yearsjob'],
            'industry_experience' => $request['experiencejob'],
        ]);

        if($jobs){
            return back()->with('updates',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
    }


    public function sectorUpdate(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $iduser=$request['id'];

        for ($x=0; $x < count( $request['sector']); $x++) { 
            for ($i=0; $i < count($request['norms']) ; $i++) { 
                $companysector = CompanySector::where('sector_id',$request['sector'][$x])->where('norm_id',$request['norms'][$i])->first();
                $validation = AuditorSector::where('company_sector_id',$companysector->id)->Where('user_id',$iduser)->first();
                if(!$validation){
                    $normandsectors=AuditorSector::create([
                        'company_sector_id' => $companysector->id,
                        'user_id'           => $iduser,
                    ]);
                }
            }
        }

        return back()->with('updates',"Dato guardado");
    }

    public function normUpdate(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $iduser=$request['id'];

        for ($i=0; $i < count($request['norm']) ; $i++) { 
            for ($x=0; $x < count( $request['sector']); $x++) { 
                $companysector = CompanySector::where('sector_id',$request['sector'][$x])->where('norm_id',$request['norm'][$i])->first();

                $validation = AuditorSector::where('company_sector_id',$companysector->id)->Where('user_id',$iduser)->first();
                if(!$validation){
                    $normandsectors=AuditorSector::create([
                        'company_sector_id' => $companysector->id,
                        'user_id'           => $iduser,
                    ]);
                }
              
            }
        }

        return back()->with('updates',"Dato guardado");
    }


    public function saveCourse(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        if($request->hasFile('filecourse')){
            $doc      = $request->file('filecourse');
            $document = File::saveFileCourse($doc);
            $name     = $document->fullPath;
        }else{
            $name     = "Sin Certificado";
        }
        $academic=HistoryAcademic::create([
            'institution'               => $request['namecourse'],
            'date'                      => $request['datecourse'],
            'carrier'                   => $request['passcourse'],
            'score'                     => $request['scorecourse'],
            'certification_institution' => $request['titlecertificate'],
            'file_path'                 => $name,
            'user_id'                   => $request['user_id']
        ]);

        if($academic){
            return back()->with('success',"Dato guardado");
        }else{
            return back()->with('error',"Ocurrio un error");
        }
    }
    public function saveFilecourse(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        if($request->hasFile('filecourse')){
            $doc      = $request->file('filecourse');
            $document = File::saveFileCourse($doc);
            $name     = $document->fullPath;
        }else{
            $name     = "Sin certificado";
        }
        if($request->course_id){
            $academic = HistoryAcademic::find($request['course_id']);
            $academic->update([
                'file_path' => $name,
            ]);
        }
        return $name;
    }

    public function deleteSector(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $auditsector = AuditorSector::where('user_id',$request['user_id'])->get();

        foreach ($auditsector as $sectorcompany) {
            if($sectorcompany->company->sector_id==$request['id']){
                $eliminar = AuditorSector::find($sectorcompany->id);
                $eliminar->delete();
            }
        }

        return back()->with('delete',"Datos eliminados");

    }

    public function deleteNorm(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $auditsector = AuditorSector::where('user_id',$request['user_id'])->get();

        foreach ($auditsector as $normcompany) {
            if($normcompany->company->norm_id==$request['id']){
                $eliminar = AuditorSector::find($normcompany->id);
                $eliminar->delete();
            }
        }

        return back()->with('delete',"Datos eliminados");
    }

    public function deleteCourse(Request $request)
    {
        $academic = HistoryAcademic::find($request['id']);
        if($academic->file_path!="Sin certificado"){
            $path = public_path().'/storage/'.$academic->file_path;
            unlink($path);
        }
        $academic->delete();

        return back()->with('delete',"Datos eliminados");

    }
    public function deleteHistory(Request $request)
    {
        $audit = AuditHistory::find($request['id']);
        $audit->delete();

        if($audit){
            return back()->with('delete',"Datos eliminados");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
    }

    public function saveHistory(Request $request)
    {
        $historial = AuditHistory::create([
            'name_company' =>$request['name_organization'],
            'init_date'    =>$request['date_first'],
            'finish_date'  =>$request['date_end'],
            'sector'       =>$request['sector'],
            'norm'         =>$request['norma'],
            'roll'         =>$request['rol'],
            'user_id'      =>$request['id']
        ]);
        return back()->with('success',"Dato guardado");
    }

    public function deleteJobDetail(Request $request)
    {
        switch ($request['context']) {
            case 1:
                $detalledelete = JobHistory::find($request['id']);
                $detalledelete->delete();
                break;

            case 2:
                $detalledelete = YearceHistory::find($request['id']);
                $detalledelete->delete();
                break;

            case 3:
                $detalledelete = ExperienceHistory::find($request['id']);
                $detalledelete->delete();
                break;

            case 4:
                $detalledelete = EvaluationHistory::find($request['id']);
                $detalledelete->delete();
                break;                                            
            
        }

        return "Datos eliminados";

    }

    public function addJobDetail(Request $request)
    {
        $iduser = $request['id'];
        if(isset($request->Periodojobhistory)){
            for ($i=0; $i < count($request->Periodojobhistory); $i++) { 
                $jobhistory = JobHistory::create([
                    'period'             =>$request['Periodojobhistory'][$i],
                    'company'            =>$request['Empresajobhistory'][$i],
                    'position'           =>$request['Puestojobhistory'][$i],
                    'developed_activity' =>$request['Actividadjobhistory'][$i],
                    'user_id'            =>$iduser
                ]);
            }
        }

        if(isset($request->Empresaauditrealizated)){
            for ($i=0; $i < count($request->Empresaauditrealizated); $i++) { 
                $jobhistory = YearceHistory::create([
                    'company' =>$request['Empresaauditrealizated'][$i],
                    'type'    =>$request['Tipoauditrealizated'][$i],
                    'norm'    =>$request['Normaauditrealizated'][$i],
                    'date'    =>$request['Fechaauditrealizated'][$i],
                    'days'    =>$request['Diasauditrealizated'][$i],
                    'sector'  =>$request['Sectorauditrealizated'][$i],
                    'user_id' =>$iduser
                ]);
            }
        }

        if(isset($request->Nombreformation)){
            for ($i=0; $i < count($request->Nombreformation); $i++) { 
                $experience = ExperienceHistory::create([
                    'course'   =>$request['Nombreformation'][$i],
                    'hours'    =>$request['Horasformation'][$i],
                    'date'     =>$request['Fechaformation'][$i],
                    'days'     =>$request['Díasformation'][$i],
                    'imparted' =>$request['Impartidoformation'][$i],
                    'user_id'  => $iduser
                ]);
            }
        }


        if(isset($request->Empresaevaluation)){
            for ($i=0; $i < count($request->Empresaevaluation); $i++) { 
                $evaluation = EvaluationHistory::create([
                    'company'              =>$request['Empresaevaluation'][$i],
                    'date'                 =>$request['Fechaevaluation'][$i],
                    'type'                 =>$request['Tipoevaluation'][$i],
                    'period'               =>$request['Periodoevaluation'][$i],
                    'qualification'        =>$request['Calificacionevaluation'][$i],
                    'evaluation'           =>$request['Testificaciónevaluation'][$i],
                    'client_qualification' =>$request['CalificacioClientenevaluation'][$i],
                    'user_id'                  => $iduser,
                    'qualificationfile'        =>$request['archivoCalificacion'][$i],
                    'client_qualificationfile' =>$request['archivoCalificacionCliente'][$i],

                ]);
            }
        }   

        return back()->with('success',"Dato guardado");

    }
}
