<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionChecklist extends Model
{
    public $timestamps  = true;

	protected $table    = 'section_checklists';

	protected $fillable = ['code','description','order'];   

	protected $guarded  = ['id'];
}
