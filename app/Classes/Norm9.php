<?php

namespace App\Classes;

use Illuminate\Database\Eloquent\Model;
use App\SectionChecklist;
use App\Checklist;

class Norm9 extends Model
{
	public static function check($id)
    {
		$section4  = SectionChecklist::where('code','4')->first();
		$section5  = SectionChecklist::where('code','5')->first();
		$section6  = SectionChecklist::where('code','6')->first();
		$section7  = SectionChecklist::where('code','7')->first();
		$section8  = SectionChecklist::where('code','8')->first();
		$section9  = SectionChecklist::where('code','9')->first();
		$section10 = SectionChecklist::where('code','10')->first();

    	$checklist = Checklist::create([
            'code'                 => '4.1',
			'description'          => 'Comprensión del contexto de la organización ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo determina las cuestiones externas e internas pertinentes para su propósito y su dirección estratégica que afectan el logro de objetivos de su SGC? ',
]);
 	

    	$checklist = Checklist::create([
        	'code'                 => '4.2',
			'description'          => 'Comprensión de las necesidades y expectativas de las partes interesadas',
			'order'                => '2',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cuáles partes interesadas son pertinentes para su sistema de gestión de la calidad? ¿Cuáles requisitos de las partes interesadas son pertinentes para el SGC? ¿Cómo da seguimiento y revisa la información pertinente de los requisitos de las partes interesadas y sus requisitos pertinentes? ',

        ]);


    	$checklist = Checklist::create([
			'code'                 => '4.3',
			'description'          => 'Determinación del alcance del SGC ',
			'order'                => '3',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',			
			'help'				   => '¿Cómo determinó los limites y aplicabilidad de su SGC? ¿Cómo considero los siguientes puntos para determinar el alcance de su SGC? a) las cuestiones externas e internas indicadas en el  apartado 4.2 b) Los requisitos de las partes interesadas pertinentes c) Los productos y servicios de la organización ¿Dónde se tiene disponible como información documentada el alcance del SGC? ¿Cuáles productos y/o servicios están considerados en el alcance del SGC? ¿Cuáles requisitos se excluyeron del SGC?',
        ]);


    	$checklist = Checklist::create([
			'code'                 => '4.4.1',
			'description'          => 'Sistema de gestión de calidad y sus procesos ',
			'order'                => '4',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',			
			'help'				   => '¿Como se ha establecido, implementado y mantenido su SGC  icluyendo procesos e interracciones necesarios? ¿Cómo determinó los procesos necesarios y su interacción en el SGC incluyendo lo siguiente? a) determinación de las entradas requeridas y los resultados esperados de estos  procesos; b) determinación de la secuencia e interacción de estos procesos; c) determinación  y aplicación de  los criterios y los métodos (incluyendo el seguimiento, las mediciones y los indicadores del desempeño relacionados) necesarios para asegurarse de la operación eficaz y el control de estos procesos; d) determinación de los recursos necesarios para estos procesos y  su  disponibilidad; e) asignación de las responsabilidades y autoridades para estos procesos;  f) abordar los riesgos y oportunidades determinados de acuerdo con los requisitos del apartado 6.1; g) evaluación de estos procesos e implementar cualquier cambio necesario para asegurarse de que estos procesos logran los resultados previstos; h) mejora de los procesos y el sistema de gestión de la calidad',
        ]);


    	$checklist = Checklist::create([
			'code'                 => '4.4.2',
			'description'          => '¿Cómo mantiene  la información documentada que apoya la operación de sus procesos? ',
			'order'                => '5',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo retiene la información documentada para tener confianza de que los procesos son llevados a cabo como se han planeado?',
        ]);

    	// ----------------------------------------------
    	$checklist = Checklist::create([
			'code'                 => '5.1.1',
			'description'          => 'Liderazgo y compromiso General ',
			'order'                => '6',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo demuestra, la alta dirección, su liderazgo y compromiso con respecto a: a)  la responsabilidad y obligación de rendir cuentas con relación a la eficacia del SGC; b) establecimiento de la política d y los objetivos de la calidad para el SGC, y que éstos sean compatibles con el contexto y la dirección estratégica de la organización; c) asegurándo la integración de los requisitos del SGC en los procesos de negocio de la organización; d)  el uso del enfoque a procesos y el pensamiento basado en riesgos; e) asegurándo que los recursos necesarios para el SGC estén disponibles; f) Comunicando la importancia de una gestión de la calidad eficaz y conforme con los requisitos del SGC; g) asegurándo  que el SGC logre los resultados  previstos; h) Comprometiendo, dirigiendo y apoyando a las personas, para contribuir a la eficacia del SGC. i) Promoviendo la mejora; j) Apoyando otros roles pertinentes de la dirección, para demostrar su liderazgo en la forma en la que aplique a sus áreas de responsabilidad.',
        ]);
        $checklist = Checklist::create([
			'code'                 => '5.1.2',
			'description'          => 'Enfoque al cliente ',
			'order'                => '7',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo demuestra, la alta dirección, su liderazgo y compromiso con respecto a que: a) se determinen, se comprenden y se cumplen regularmente los requisitos del cliente y los legales y reglamentarios aplicables; b) se determinen y se consideran los riesgos y oportunidades que pueden afectar a la conformidad de los productos y servicios y a la capacidad de aumentar la satisfacción del cliente; c) se mantenga el enfoque en el aumento de la satisfacción del cliente',
        ]);
        $checklist = Checklist::create([
			'code'                 => '5.2.1',
			'description'          => 'Política Establecimiento de la política de calidad ',
			'order'                => '8',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo establece, implementa y mantiene una politica de calidad basando en que: a) sea apropiada al propósito y contexto de la organización y apoye su dirección estratégica; b) proporcione un marco de referencia para el establecimiento de los objetivos de la calidad; c) incluya un compromiso de cumplir los requisitos aplicables; d) incluya un compromiso de mejora continua del sistema de gestión de la calidad.',
        ]);
        $checklist = Checklist::create([
			'code'                 => '5.2.2',
			'description'          => 'Comunicación de la política de calidad ',
			'order'                => '9',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo  asegura la correcta comunicación de la politica de calidad? considerando que debe: a) estar disponible y mantenerse como información documentada; b) comunicarse, entenderse y aplicarse dentro de la organización; c) estar disponible para las partes interesadas pertinentes, según corresponda',
        ]);
        $checklist = Checklist::create([
			'code'                 => '5.3',
			'description'          => 'Organización, roles, responsabilidades y autoridades ',
			'order'                => '10',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo se han asignado responsabilidades y autoridades para: a) asegurarse de que el SGC es conforme con los requisitos de esta Norma Internacional; b) asegurarse de que los procesos están generando y proporcionando los resultados previstos; c) informar, en particular, a la alta dirección sobre el desempeño del SGC y sobre las oportunidades de mejora (véase 10.1); d) asegurarse de que se promueve el enfoque al cliente en toda la organización; e) asegurarse de que la integridad del SGC se mantiene cuando se planifican e implementan cambios SGC',
        ]);

        // --------------------------------------

        $checklist = Checklist::create([
			'code'                 => '6.1.1',
			'description'          => 'Acciones para abordar riesgos y oportunidades ',
			'order'                => '11',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo se determinaron riesgos y oportunidades necesarios para: a) asegurar que el SGC logre los resultados  previstos; b) aumente los efectos deseables; c) prevenga o reduzcar efectos no deseados; d) Logre la mejora?',
        ]);
        $checklist = Checklist::create([
			'code'                 => '6.1.2',
			'description'          => '¿ Cómo se planifican las acciones para:poder abordar riesgos y oportunidades al igual que: ',
			'order'                => '12',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'help'				   => '1) integrar e implementar las acciones en sus procesos del SGC; 2) evaluar la eficacia de estas acciones?',
        ]);
        $checklist = Checklist::create([
			'code'                 => '6.2.1',
			'description'          => 'Objetivos de calidad y planeación para alcanzarlos',
			'order'                => '13',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'help'				   => ' ¿Cuales objetivos se tienen para cada función y nivel pertinente y de los procesos necesarios para el SGC? ¿Estos objetivos cumplen con las siguientes caracteristicas: a) ser coherentes con la política de la calidad; b) ser medibles; c) tener en cuenta los requisitos aplicables; d) ser pertinentes para la conformidad de los productos y servicios y para el aumento de la satisfacción del cliente; e) ser objeto de seguimiento; f) comunicarse; g) actualizarse, según corresponda?',
        ]);
        $checklist = Checklist::create([
			'code'                 => '6.2.2',
			'description'          => '¿ Cómo se  planifica el logro de sus objetivos de calidad con base en:',
			'order'                => '14',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) qué se va a hacer; b) qué recursos se requerirán; c) quién será responsable; d) cuándo se finalizará; e) cómo se evaluarán los resultados?',
        ]);
        $checklist = Checklist::create([
			'code'                 => '6.3',
			'description'          => 'Planeación del cambio ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'help'				   => '¿Cómo se planean los cambios necesarios en el SGC  considerando lo siguiente: a)el propósito de los cambios y sus consecuencias potenciales; b)la integridad del sistema de gestión de la calidad; c)la disponibilidad de recursos; d)la asignación o reasignación de responsabilidades y autoridades?',
        ]);

        // ---------------------------------------------
                $checklist = Checklist::create([
			'code'                 => '7.1.1',
			'description'          => '¿Cómo se consideran los recursos necesarios para el establecimiento, implementación, mantenimiento y mejora continua del SGC  tomando en cuenta.',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) las capacidades y limitaciones de los recursos internos existentes;b) qué se necesita obtener de los proveedores externos.',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.2',
			'description'          => '¿Cómo determina al personal necesario para la implementacion eficaz de su SGC, operación y control de procesos?',
			'order'                => '16',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.3',
			'description'          => '¿Cuál infraestructura se tiene para la operación de los procesos y poder lograr la conformidad de los productos y servicios incluyendo:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) edificios y servicios asociados;b) equipos, incluyendo hardware y software;c) recursos de transporte;d) tecnologías de la información y la  comunicación',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.4',
			'description'          => '¿Cómo mantienel ambiente de trabajo necesario (social, psicologico, físico) para la operación y conformidad de sus productos?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.5.1',
			'description'          => '¿Cómo asegura la validez y confiabilidad de los resultados cuando se realiza el seguimiento o la medición para verificar la conformidad de los productos y servicios con los requisitos?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.5.1',
			'description'          => '¿Que características deben tenerr los recursos proporcionados para un correcto seguimiento y medición de estos:?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) son apropiados para el tipo específico de actividades de seguimiento y medición realizadas;
									   b) se mantienen para asegurarse de la idoneidad continua para su propósito',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.5.1',
			'description'          => '¿Como se lleva a cabo su registro de información documentada para evidencia de recursos de seguimiento y medición sean idóneos para su propósito?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.5.2',
			'description'          => '¿Cómo se tiene la trazabilidad de las mediciones para proporcionar confianza en la validez de sus resultados y el equipo de medición: calibrando o verificando a intervalos especificados, o antes de su utilización, contra patrones de medición trazables a patrones de medición internacionales o nacionales?;',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => 'Patrones, ¿como se conserva la información documentada de la base utilizada para la calibración o la verificación, cuando no existan tales patrones?; para ambos se considera. a)identificarse para determinar su  estado;b) protegerse contra ajustes, daño o deterioro que pudieran invalidar el estado de calibración y los posteriores resultados de la medición',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.1.6',
			'description'          => '¿Cómo se determinan los conocimientos necesarios para la operación de sus procesos y  para lograr la conformidad de los productos y servicios? basado en:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) fuentes internas (por ejemplo, propiedad intelectual; conocimientos adquiridos con la experiencia; lecciones aprendidas de los fracasos y de proyectos de éxito; capturar y compartir conocimientos y experiencia no documentados; los resultados de las mejoras en los procesos, productos y  servicios. b) fuentes externas (por ejemplo, normas; academia; conferencias; recopilación de conocimientos provenientes de clientes o proveedores externos).',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.2',
			'description'          => 'La organización asegura su competencia de acuerdo a:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) determinar la competencia necesaria de las personas que realizan, bajo su control, un trabajo que afecta al desempeño y eficacia del sistema de gestión de la calidad;b) asegurarse de que estas personas sean competentes, basándose en la educación, formación o experiencia apropiadas; c) cuando sea aplicable, tomar acciones para adquirir la competencia necesaria y evaluar la eficacia de las acciones tomadas; d) conservar la información documentada apropiada como evidencia de la competencia.',
        ]);
        
        $checklist = Checklist::create([
			'code'                 => '7.3',
			'description'          => 'Como se asegura que las personas que realizan el trabajo bajo el control de la organización tomen conciencia:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) la política de la calidad;b) los objetivos de la calidad pertinentes;c) su contribución a la eficacia del sistema de gestión de la calidad, incluidos los beneficios de una mejora del desempeño; d) las implicaciones del incumplimiento de los requisitos del sistema de gestión de la calidad.',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.4',
			'description'          => 'La comunicación interna y externa pertinente al SGC debe de incluir:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a)qué comunicar;b)cuándo comunicar;c)a quién comunicar;d)cómo comunicar;e)quién comunica.',
        ]);

        $checklist = Checklist::create([
			'code'                 => '7.5.1',
			'description'          => '¿Dónde se tiene la información documentada para el SGC, que incluya: ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) la información documentada requerida por esta Norma Internacional;b) la información documentada que la organización determina como necesaria para la eficacia del sistema de gestión de la calidad.',
        ]);
        $checklist = Checklist::create([
			'code'                 => '7.5.2',
			'description'          => '¿Como se crea y actualiza la información documentada y se asegura lo siguiente: ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => '  a) la identificación y descripción (por ejemplo, título, fecha, autor o número de referencia);b) el formato (por ejemplo, idioma, versión del software, gráficos) y los medios de soporte (por ejemplo, papel, electrónico);c) la revisión y aprobación con respecto a la conveniencia y adecuación.',
        ]);
        $checklist = Checklist::create([
			'code'                 => '7.5.3.1',
			'description'          => '¿Como se mantiene controlada la información documentada para asegurar que: ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) esté disponible y sea idónea para su uso, donde y cuando se necesite;b) esté protegida adecuadamente (por ejemplo, contra pérdida de la confidencialidad, uso inadecuado o pérdida de integridad).',
        ]);
        $checklist = Checklist::create([
			'code'                 => '7.5.3.2',
			'description'          => '¿Cómo se realizan las siguientes actividades para mantener el control de la información documentada :',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) distribución, acceso, recuperación y uso; b) almacenamiento y preservación, incluida la preservación de la legibilidad;c) control de cambios (por ejemplo, control de versión); d) conservación y disposición?',
        ]);

        // ---------------------------------------------

        $checklist = Checklist::create([
			'code'                 => '8.1',
			'description'          => '¿Cómo planifica, implementa y controla los procesos  necesarios para cumplir los requisitos para la provisión de productos y servicios, y para implementar las acciones a traves de:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) la determinación de los requisitos para los productos y servicios;b) el establecimiento de criterios para los procesos y la aceptación de productos y serviciosc) la determinación de los recursos necesarios para lograr la conformidad con los requisitos de los productos y servicios;d) la implementación del control de los procesos de acuerdo con los criterios;e) la determinación, el mantenimiento y la conservación de la información documentada en la extensión necesaria para:1) tener confianza en que los procesos se han llevado a cabo según lo planificado;2) demostrar la conformidad de los productos y servicios con sus requisitos?',
        ]);
        $checklist = Checklist::create([
			'code'                 => '8.1',
			'description'          => '¿Cómo asegura la organización que los procesos contratados externamente estén controlados',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);
        $checklist = Checklist::create([
			'code'                 => '8.2.2',
			'description'          => '¿Dónde se definen los requisitos para productos y servicios referente a comunicación con el cliente que incluyan:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) proporcionar la información relativa a los productos y servicios;b)tratar las consultas, los contratos o los pedidos, incluyendo los cambios;c)obtener la retroalimentación de los clientes relativa a los productos y servicios, incluyendo las quejas de los clientes;d)manipular o controlar la propiedad del cliente;e)establecer los requisitos específicos para las acciones de contingencia, cuando sea  pertinente?',
        ]);  
        $checklist = Checklist::create([
			'code'                 => '8.2.3.1',
			'description'          => '¿Cómo revisa los requisitos antes de comprometerse a suministrar productos y servicios a un cliente  incluyendo :?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) los requisitos especificados por el cliente, incluyendo los requisitos para las actividades de entrega y las posteriores a la misma;b) los requisitos no establecidos por el cliente, pero necesarios para el uso especificado o previsto, cuando sea conocido;c) los requisitos especificados por la organización;d) los requisitos legales y reglamentarios aplicables a los productos y servicios;e) las diferencias existentes entre los requisitos del contrato o pedido y los expresados  previamente',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.2.3.2',
			'description'          => 'La información documentada de la organización es aplicable a:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a)sobre los resultados de la revisión;b)sobre cualquier requisito nuevo para los productos y servicios.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.2.4',
			'description'          => 'Cuando se cambien los requisitos para los productos y servicios, la informacion documentada pertinente debe se modificada y tambien las personas involucradas sean conscientes de los requisitos modificados.',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.3.1',
			'description'          => 'La organización debe establecer, implementar y mantener un proceso de diseño y desarrollo que sea adecuado para asegurarse de la posterior provisión de productos y servicios',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '8.3.2',
			'description'          => 'Para la determinacion de las etapas y controles de la planificacion del diseño y desarrollo se debe de considerar:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) la naturaleza, duración y complejidad de las actividades de diseño y desarrollo;b) las etapas del proceso requeridas, incluyendo las revisiones del diseño y desarrollo aplicables;c) las actividades requeridas de verificación y validación del diseño y desarrollod) las responsabilidades y autoridades involucradas en el proceso de diseño y desarrollo;e) las necesidades de recursos internos y externos para el diseño y desarrollo de los productos y servicios;f) la necesidad de controlar las interfaces entre las personas que participan activamente en el proceso de diseño y desarrollo;g) la necesidad de la participación activa de los clientes y usuarios en el proceso de diseño y desarrollo;h) los requisitos para la posterior provisión de productos y servicios;i) el nivel de control del proceso de diseño y desarrollo esperado por los clientes y otras partes interesadas pertinentes;j) la información documentada necesaria para demostrar que se han cumplido los requisitos del diseño y desarrollo.',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '8.3.3',
			'description'          => 'Algunos de los requisitos esenciales para los tipos específicos de productos y servicios a diseñar y desarrollar se deben considerar:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) los requisitos funcionales y de desempeño;b) la información proveniente de actividades previas de diseño y desarrollo similares;c) los requisitos legales y reglamentarios;d) normas o códigos de prácticas que la organización se ha comprometido a implementar;e) las consecuencias potenciales de fallar debido a la naturaleza de los productos y servicios.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.3.4',
			'description'          => 'La organización debe aplicar controles al proceso de diseño y desarrollo para asegurarse de que:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) se definen los resultados a lograr;b) se realizan las revisiones para evaluar la capacidad de los resultados del diseño y desarrollo para cumplir los requisitos;c) se realizan actividades de verificación para asegurarse de que las salidas del diseño y desarrollo cumplen los requisitos de las entradas;d) se realizan actividades de validación para asegurarse de que los productos y servicios resultantes satisfacen los requisitos para su aplicación especificada o uso  previsto;e) se toma cualquier acción necesaria sobre los problemas determinados durante las revisiones, o las actividades de verificación y validación;f) se conserva la información documentada de estas actividades.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.3.5',
			'description'          => 'Las salidas del diseño y desarrollo de la organización  debe asegurar que:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a)cumplen los requisitos de las entradas;b)son adecuadas para los procesos posteriores para la provisión de productos y servicios;c)incluyen o hacen referencia a los requisitos de seguimiento y medición, cuando sea apropiado, y a los criterios de aceptación;d)especifican las características de los productos y servicios que son esenciales para su propósito previsto y su provisión segura y correcta.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.3.5',
			'description'          => 'Se debe mantener informacion documentada sobre las salidas de diseño y desarrollo',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.3.6',
			'description'          => 'La organización debe conservar informacion documentada sobre los cambios del diseño y desarrollo sobre:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) los cambios del diseño y desarrollo;b) los resultados de las revisiones;c) la autorización de los cambios;d) las acciones tomadas para prevenir los impactos adversos.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.4.1',
			'description'          => 'La organización debe determinar los controles a aplicar a los procesos, productos y servicios suministrados externamente cuando:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) los productos y servicios de proveedores externos están destinados a incorporarse dentro de los propios productos y servicios de la organización;b) los productos y servicios son proporcionados directamente a los clientes por proveedores externos en nombre de la organización;c) un proceso, o una parte de un proceso, es proporcionado por un proveedor externo como resultado de una decisión de la organización',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.4.2',
			'description'          => 'La organización debe asegurarse de que los procesos, productos y servicios suministrados ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a)externamente no afectan de manera adversa a la capacidad de la organización de entregar productos y b)servicios conformes de manera coherente a sus clientes.La organización debe:a) asegurarse de que los procesos suministrados externamente permanecen dentro del control de su sistema de gestión de la calidad;b) definir los controles que pretende aplicar a un proveedor externo y los que pretende aplicar a las salidas resultantes;c) tener en consideración:1)el impacto potencial de los procesos, productos y servicios suministrados externamente en la capacidad de la organización de cumplir regularmente los requisitos del cliente y los legales y reglamentarios aplicables;2)la eficacia de los controles aplicados por el proveedor externo;d) determinar la verificación, u otras actividades necesarias para asegurarse de que los procesos, productos y servicios suministrados externamente cumplen los requisitos.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.4.3',
			'description'          => 'La organización debe comunicar a los proveedores externos sus requisitos para:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) los procesos, productos y servicios a proporcionar;b) la aprobación de:1) productos y servicios;2) métodos, procesos y equipos 3) la liberación de productos y servicios;c) la competencia, incluyendo cualquier calificación requerida de las personas;d) las interacciones del proveedor externo con la organización;e) el control y el seguimiento del desempeño del proveedor externo a aplicar por parte de la organización;f) las actividades de verificación o validación que la organización, o su cliente, pretende llevar a cabo en las instalaciones del proveedor externo.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.1',
			'description'          => 'Las condiciones controladas para la poduccion y porvision de servicios deben incluir, cuando sea aplicable:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) la disponibilidad de información documentada que defina:1) las características de los productos a producir, los servicios a prestar, o las actividades a desempeñar;2) los resultados a alcanzar;b) la disponibilidad y el uso de los recursos de seguimiento y medición adecuados;c) la implementación de actividades de seguimiento y medición en las etapas apropiadas para verificar que se cumplen los criterios para el control de los procesos o sus salidas, y los criterios de aceptación para los productos y servicios;d) el uso de la infraestructura y el entorno adecuados para la operación de los procesos;e) la designación de personas competentes, incluyendo cualquier calificación requerida;f) la validación y revalidación periódica de la capacidad para alcanzar los resultados planificados de los procesos de producción y de prestación del servicio, cuando las salidas resultantes no puedan verificarse mediante actividades de seguimiento o medición posteriores;g) la implementación de acciones para prevenir los errores humanos;h) la implementación de actividades de liberación, entrega y posteriores a la entrega.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.2',
			'description'          => 'Como identifica las salidas para asegurar la conformidad de productos y servicios respecto a seguimiento y medicion a traves de la produccion y prestacion del servicio?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.2',
			'description'          => 'Como identifica la trazabilidad de sus salidas en su informacion documentada?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.3',
			'description'          => 'Como resguarda, verifica y protege la propiedad perteneciente a los clientes o provedores externos?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.4',
			'description'          => 'Las salidas durante la produccion y prestacion del servicio como lleva a cabo su preservacion para asegurarse de la conformidad con los requisitos?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.5',
			'description'          => 'c) la competencia, incluyendo cualquier calificación requerida de las personas;',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.5',
			'description'          => 'Al determinar el alcance de las actividades posteriores a la entrega que se requieren, la organización debe considerar:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a)los requisitos legales y reglamentarios;b)las consecuencias potenciales no deseadas asociadas a sus productos y servicios;c)la naturaleza, el uso y la vida útil prevista de sus productos y servicios;d)los requisitos del cliente;e)la retroalimentación del cliente.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.5.6',
			'description'          => 'Como lleva a cabo su control de los cambios para la produccion o prestacion del servicio para asegurar la continuidad en la conformidad con los requisitos?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.6',
			'description'          => 'La liberación de los productos y servicios al cliente no debe llevarse a cabo hasta que se hayan completado satisfactoriamente las disposiciones planificadas, a menos que sea aprobado de otra manera por una autoridad pertinente y, cuando sea aplicable, por el cliente.La organización debe conservar la información documentada sobre la liberación de los productos y servicios. La información documentada debe incluir:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a)evidencia de la conformidad con los criterios de aceptación;b)trazabilidad a las personas que autorizan la liberación.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.7.1',
			'description'          => 'Como asegura la organizacion de que los productos que no sean conformes con sus requisitos se identifican y se controlan para prevenir su uso o entrega no intencionada?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.7.1',
			'description'          => 'Esto se debe aplicar también a los productos y servicios no conformes detectados después de la entrega de los productos, durante o después de la provisión de los servicios.',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'La organización debe tratar las salidas no conformes de una o más de las siguientes maneras:a)corrección;b)separación, contención, devolución o suspensión de provisión de productos y servicios;c)información al cliente;d)obtención de autorización para su aceptación bajo concesión.Debe verificarse la conformidad con los requisitos cuando se corrigen las salidas no conformes',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '8.7.2',
			'description'          => 'La organización debe conservar la información documentada que:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) describa la no conformidad;b) describa las acciones tomadas;c) describa todas las concesiones obtenidas;d) identifique la autoridad que decide la acción con respecto a la no conformidad.',
        ]); 

        // ----------------------------------------------------------
        $checklist = Checklist::create([
			'code'                 => '9.1.1',
			'description'          => 'De acuerdo a la evaluacion del desempe;o del seguimineto, medicion, analisis y evaluacion la organización debe determinar:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) qué necesita seguimiento y medición;b) los métodos de seguimiento, medición, análisis y evaluación necesarios para asegurar resultados válidos;c) cuándo se deben llevar a cabo el seguimiento y la medición;d) cuándo se deben analizar y evaluar los resultados del seguimiento y la medición.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.1.2',
			'description'          => 'Como determina su organizacion los métodos para obtener, realizar el seguimiento y revisar la informacion del la satisfaccion del cliente?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.1.3',
			'description'          => 'El analisis y evaluacion de los datos de la informacion apropiados surgen por el seguimiento y la medicion los resultados del analisis deben utilizarse para evaluar;',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => 'a) la conformidad de los productos y servicios;b) el grado de satisfacción del cliente;c) el desempeño y la eficacia del sistema de gestión de la calidad;d) si lo planificado se ha implementado de forma eficaz;e) la eficacia de las acciones tomadas para abordar los riesgos y oportunidades;f) el desempeño de los proveedores externos;g) la necesidad de mejoras en el sistema de gestión de la calidad.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.2.1',
			'description'          => '¿Tiene un programa de auditorías internas?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => '¿Cómo se determina la frecuencia de auditorías internas?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => 'Cada auditoría tiene un alcance definido y considera una actividad o sección definida del SGC?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => 'Se demuestra la competencia y capacitación de los auditores involucrados en el proceso de auditoría?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => 'Realiza las acciones y toman las acciones correctivas adecuadas sin demora injustificada?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => 'El programa de auditoría interna se implementa completamente?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]);   
        
        $checklist = Checklist::create([
			'code'                 => '9.2.2',
			'description'          => 'Como conservan la informacion documentada de la evidencia de la implementacion de la auditoria y  los resultados de la misma?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        
        $checklist = Checklist::create([
			'code'                 => '9.3.1',
			'description'          => 'Cada que tiempo se tiene planificado hacer la revision por la direccion ?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.3.2',
			'description'          => 'La revisión por la dirección debe planificarse y llevarse a cabo considerando:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) el estado de las acciones de las revisiones por la dirección previas;b)los cambios en las cuestiones externas e internas que sean pertinentes al sistema de gestión de la calidad;c)la información sobre el desempeño y la eficacia del sistema de gestión de la calidad;d)la adecuación de los recursos;e)la eficacia de las acciones tomadas para abordar los riesgos y las oportunidades (véase 6.1);f)las oportunidades de mejora.',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '9.3.2',
			'description'          => 'Se debe considerar el desempe;o y la eficacia del SGC sobre las tendencias relativas a ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => '1)la satisfacción del cliente y la retroalimentación de las partes interesadas pertinentes;2)el grado en que se han logrado los objetivos de la calidad;3)el desempeño de los procesos y conformidad de los productos y servicios;4)las no conformidades y acciones correctivas;5)los resultados de seguimiento y medición;6)los resultados de las auditorías;7)el desempeño de los proveedores externos;',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '9.3.3',
			'description'          => 'Las salidas de la revisión por la dirección deben incluir las decisiones y acciones relacionadas con:',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'help'				   => ' Las salidas de la revisión por la dirección deben incluir las decisiones y acciones relacionadas con:a)las oportunidades de mejora;b)cualquier necesidad de cambio en el sistema de gestión de la calidad;c)las necesidades de recursos.',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '10.1',
			'description'          => 'Han detectado alguna oportunidad de mejora dentro de su proceso?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '10.1',
			'description'          => 'Cuando existe alguna oportunidad de mejora debe incluir lo siguiente',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => ' a) mejorar los productos y servicios para cumplir los requisitos, así como considerar las necesidades y expectativas futuras;b) corregir, prevenir o reducir los efectos no deseados;c) mejorar el desempeño y la eficacia del sistema de gestión de la calidad.',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '10.2',
			'description'          => 'Que acciones toman ante una no conformidad para controlarla y corregirla?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '10.2',
			'description'          => 'Que metodo utilizan para evaluar la necesidad de acciones para eliminar las causas de la no conformidad, con el fin de que no vuelva a ocurrir ?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
        $checklist = Checklist::create([
			'code'                 => '10.2.2',
			'description'          => 'Como identifican la informacion documenta para las no conformidades y las acciones correctivas tomadas posteriormente ',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 

        $checklist = Checklist::create([
			'code'                 => '10.3',
			'description'          => 'Como evalua la eficacia del SGC para una mejora continua?',
			'order'                => '15',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'help'				   => '',
        ]); 
 	}   
}

