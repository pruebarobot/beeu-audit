<?php

namespace App\Classes;

use App;
use Illuminate\Database\Eloquent\Model;
use App\Classes\Norm9;
use App\Classes\Norm14;
use App\Classes\Norm45;

class Selection extends Model
{
    public static function check($id,$norm)
    {
    	switch ($norm) {
    		case '9':
    			$checklist = Norm9::check($id);
    			break;

    		case '14':
    			$checklist = Norm14::check($id);
    			break;

    		case '45':
    			$checklist = Norm45::check($id);
    			break;
    	}
    }
}
