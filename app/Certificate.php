<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table    = 'certificates';
	
	protected $fillable = ['audit_id','file_path','init_date','end_date','type','status'];
	
	protected $guarded  = [ 'id' ];

	public function Audit()
	{
		return $this->hasOne('App\Audit','id','audit_id');
  	}
}
