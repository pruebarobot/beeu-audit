<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryEmployment extends Model
{
    protected $table    = 'history_employments';
	
	protected $fillable = ['job','year_experience','industry_experience','user_id'];

	public $timestamps  = true;

}
