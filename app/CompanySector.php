<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySector extends Model
{
	protected $table    = 'company_sectors';
	
	protected $fillable = [ 'sector_id','norm_id','companies_id'];
	
	protected $guarded  = [ 'id' ];

	public function sector()
	{
  		return $this->belongsTo('App\Sector');
    }

    public function norm()
	{
  		return $this->belongsTo('App\Norm');
    }
}
