<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class AuditSectorNorm extends Model
{
	public $timestamps  = true;

	protected $table    = 'audit_sector_norms';

	protected $fillable = ['sector_id','norm_id','audit_id']; 

	protected $guarded  = ['id'];

	public function sector()
	{
  		return $this->belongsTo('App\Sector');
    }

    public function norm()
	{
  		return $this->belongsTo('App\Norm');
    }

    public function checklist()
    {
		return $this->hasMany('App\ResultChecklist','audit_sector_norm_id','id');
    }

    public function statusCheck()
    {
		return $this->hasMany('App\StatusChecklist','audit_sector_norm_id','id');
    }

}
