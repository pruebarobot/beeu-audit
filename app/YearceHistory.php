<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YearceHistory extends Model
{
    protected $table    = 'yearce_histories';
	
	protected $fillable = ['company','type','norm','sector','days','date','user_id'];

	public $timestamps  = true;
}
