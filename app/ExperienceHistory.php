<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceHistory extends Model
{
    protected $table    = 'experience_histories';
	
	protected $fillable = ['course','hours','date','days','imparted','user_id'];
	
	public $timestamps  = true;
}
