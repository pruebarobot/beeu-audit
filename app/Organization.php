<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
	protected $table    = 'organizations';
	
	protected $fillable = [ 'name','rfc','certification_address_id','invoice_address_id','sites','employees','code'];
	
	protected $guarded  = [ 'id' ];

    public function directionCertification()
	{
  		return $this->belongsTo('App\Direction','certification_address_id','id');
    }
    public function directionInvoice()
	{
  		return $this->belongsTo('App\Direction','invoice_address_id','id');
    }
	public function organizationSector()
	{
		return $this->belongsTo('App\OrganizationSectorNorm','organization_id');
	}
	public function certification()
	{
		return $this->hasMany('App\Certification','organization_id');
	}

	public function organizationSectorNorm()
	{
		return $this->hasMany('App\OrganizationSectorNorm','organization_id');
	}

}
