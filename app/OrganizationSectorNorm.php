<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationSectorNorm extends Model
{
   	protected $table    = 'organization_sector_norms';
	
	protected $fillable = [ 'organization_id','company_sector_id'];
	
	protected $guarded  = [ 'id' ];

  public function organization()
	{
  		return $this->hasOne('App\Organization');
  }
  public function companies()
	{
  		return $this->belongsTo('App\CompanySector','company_sector_id','id');
  }
  public function companySertification()
  {
      return $this->belongsTo('App\CompanySector','certification_address_id','id');
  }
  public function organizationCompany()
  {
      return $this->belongsTo('App\CompanySector','company_sector_id','id');
  }
}
