<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusChecklist extends Model
{
    public $timestamps  = true;

	protected $table    = 'status_checklists';

	protected $fillable = ['audit_sector_norm_id','audit_id','status','user_id','exclusions','code','actionNoConforms','alcance'];   

	protected $guarded  = ['id'];


	public function conform()
    {
  	  return $this->hasMany('App\Conform','statuscheck_id','id');
    }

    public function user()
    {
  	  return $this->hasOne('App\User','id','user_id');
    }

    public function detalleNormaSector()
    {
  	  return $this->hasOne('App\AuditSectorNorm','id','audit_sector_norm_id');
    }
}
