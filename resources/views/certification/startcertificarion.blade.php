@foreach($organizations as $organization)
  @if($organization->certification)
    @foreach($organization->certification as $organizationdetail)

    <div class="modal fade" id="start{{$organizationdetail->id}}"  role="dialog" aria-hidden="true">
      <div class="modal-dialog-centered modal-dialog moda" role="document">
        <div class="modal-content">
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
            <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
            <div class="media align-items-center">
              
              <div id="preview" class="imagen text-center">
                <span class="tx-color-03 d-none d-sm-block"><i data-feather="flag" class="fas fa-user-plus wd-60 ht-60"></i></span>
              </div>
                  
              <div class="media-body mg-sm-l-20">
                <h4 class="tx-18 tx-sm-20 mg-b-2">INICIAR AUDITORIA</h4>
              </div>
            </div><!-- media -->

          </div><!-- modal-header -->

          <form class="form-horizontal" method="post" action="{{ route('saveaudit') }}" required="required">
            {{ csrf_field() }}
          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
              <div class="form-group">
                <label >Organizacion</label>
                <input type="hidden"   class="form-control" name="id" value="{{$organizationdetail->id}}">
                <input type="text" name="organization" id="organization" readonly disabled class="form-control" value="{{$organization->name}}">
              </div>

              <div class="form-group">
                <label >Tipo de auditoria</label>
                  <select class="form-control custom-select" style="width: 100%" id="type" name="type" required="required">
                      <option selected disabled>Elige ...</option>
                      <option value="Fase 1">Fase 1</option>
                      <option value="Fase 2">Fase 2</option>
                      <option value="Seguimiento 1">Seguimiento 1</option>
                      <option value="Seguimiento 2">Seguimiento 2</option>
                      <option value="Recertificacion">Recertificacion</option>
                      <option value="Especial">Especial</option>
                  </select>
              </div>

              <div class="form-group">
                <label >Sectores y Normas</label>
                <br>
                  <table class="table table-bordered ">
                    <tr>
                      <td>Sector</td>
                      <td>Norma</td>
                    </tr>
                    <tbody>
                        <tr>
                            <td>
                              @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                                @if($organizationdetail)
                                  <li class="{{$organizationdetail->id}}{{$sectorsandnorms->sector->id}}SectorAgregarNombre">{{$sectorsandnorms->sector->name}}</li>
                              @endif
                            @endforeach
                            </td>
                            <td>
                            @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                              @if($organizationdetail)
                                <li class="{{$organizationdetail->id}}{{$sectorsandnorms->norm->id}}NormaAgregarNombre">{{$sectorsandnorms->norm->name}}</li>
                              @endif
                              <input type="hidden" name="sectores[]" value="{{$sectorsandnorms->sector->id}}">
                              <input type="hidden" name="norms[]" value="{{$sectorsandnorms->norm->id}}">
                            @endforeach
                            </td>
                        </tr>

                  </tbody>
                </table>
              </div>

              <div class="validation">
                <div class="form-group">
                  <label >Fecha de inicio</label>
                  <input type="date" name="date_start" class="form-control date_start fechas" required="required" >
                </div>
                <div class="form-group">
                  <label >Fecha de termino</label>
                  <input type="date" name="date_finish" class="form-control date_finish fechas" required="required" >
                </div>  
              </div>

          </div>

          <div class="modal-footer pd-x-20 pd-y-15">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary btnValidation"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>
          
        </form>
    </div>
  </div>
</div>


@endforeach
@endif
@endforeach