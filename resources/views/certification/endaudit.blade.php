<div class="modal fade " id="new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content tx-14">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Auditoria finalizada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
        
        <div class="col-sm-12 mg-t-10 mg-sm-t-25" id="success">
          <div class="card card-body shadow-none bd-warning">
            <div class="marker marker-ribbon marker-warning pos-absolute t-10 l-0">Felicidades auditoria finalizada </div>
            <h6 class="mg-b-15 mg-t-30">¿Que deseas hacer?</h6>
            <p class="mg-b-0 text-center"><img src="https://4.bp.blogspot.com/-fhy6oojesRU/XM9ELNBOBCI/AAAAAAAAAVk/5FPJe5QW8EAEylym2ALUpY7ORkmrE6fqgCLcBGAs/s320/Ivhg.gif" width="200px"></p>
            <button type="button" class="btn btn-warning btn-block" id="finish"><i class="fas fa-flag-checkered"></i> Finalizar e iniciar nueva auditoria</button>
            <form method="post" action="{{route('endaudituser')}}">
              {{ csrf_field() }}
              <input type="hidden" name="auditId" value="{{$audit->id}}">
                
              <button type="submit" class="btn btn-danger btn-block" ><i class="fas fa-flag-checkered"></i> Finalizar</button>
            </form>
          </div><!-- card -->
        </div><!-- col -->

        <div id="ocul">
          <form class="form-horizontal" method="post" action="{{route('newaudit')}}" >
          {{ csrf_field() }}
            
            <h4>Iniciar nueva auditoria</h4>
            
            <br>

            <div class="form-group">
              <label >Sectores y Normas</label>
              <br>
                <table class="table table-bordered ">
                  <thead class="thead-primary">
                    <tr>
                      <td>Sector</td>
                      <td>Norma</td>
                    </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <th scope="row" >
                          <ul>
                            @foreach($audit->auditsector as $ad)
                              <li class="{{$ad->sector->id}}Sector">{{$ad->sector->name}}</li>
                            @endforeach
                          </ul>
                        </th>
                        <th scope="row">
                          <ul>
                            @foreach($audit->auditsector as $ad)
                              <li  class="{{$ad->norm->id}}Norma">{{$ad->norm->name}}</li>
                              <input type="hidden" name="sectores[]" value="{{$ad->sector->id}}">
                              <input type="hidden" name="norms[]" value="{{$ad->norm->id}}">
                            @endforeach
                          </ul>
                        </th>
                      </tr>
                  </tbody>
                </table>
            </div>

            <div class="form-group">
              <label >Tipo de auditoria</label>
                <select class="form-control" style="width: 100%" id="type" name="type" required="required">
                  @if($audit->type=="Fase 1")
                    <option selected disabled>Elige ...</option>
                    <option value="Fase 2">Fase 2</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Fase 2")
                    <option selected disabled>Elige ...</option>
                    <option value="Seguimiento 1">Seguimiento 1</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Seguimiento 1")
                    <option selected disabled>Elige ...</option>
                    <option value="Seguimiento 2">Seguimiento 2</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Seguimiento 2")
                    <option selected disabled>Elige ...</option>
                    <option value="Recertificacion">Recertificacion</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Recertificacion")
                    <option selected disabled>Elige ...</option>
                    <option value="Fase 1">Fase 1</option>
                  @elseif($audit->type=="Especial")
                    <option selected disabled>Elige ...</option>
                    <option value="Fase 1">Fase 1</option>
                    <option value="Fase 2">Fase 2</option>
                    <option value="Seguimiento 1">Seguimiento 1</option>
                    <option value="Seguimiento 2">Seguimiento 2</option>
                    <option value="Recertificacion">Recertificacion</option>
                  @endif
                </select>
            </div>
            
            <input type="hidden" name="auditId" value="{{$audit->id}}">
            
            <div class="form-group">
              <label >Fecha de inicio</label>
              <input type="date" name="date_start" class="form-control">
            </div>

            <div class="form-group">
              <label >Fecha de termino</label>
              <input type="date" name="date_finish" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary btn-block"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
            <button id="cancel" type="button" class="btn btn-secondary btn-block" ><i class="fas fa-undo"></i>  Cancelar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

