@extends('layouts.app')
    @section('css')
        <style type="text/css">
            .avatar{
                width: 60px;
                height:60px;
                border-radius: 13px;
            }
            .imagen img{
                width: 200px !important;
                border-radius: 10px;
              }
            #preview img{
              width:100px;
              border-radius: 13px;
            }

        </style>
    @endsection

@section('content')
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajustes</li>
          </ol>
          <br>
        </nav>

        <div class="container-fluid">
          <div class="card card-body">
            
            <div class="container-fluid row">
            <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i class="fas fa-cog"></i> Ajustes</h6>
            </div>

              <div class="container-fluid">
                  @foreach($companies as $company)
                    <form class="form-horizontal" method="post" action="{{ route('company.update',$company->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field("PUT") }}
                  <div class="modal-content">
                      
                      <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
                        <div class="media align-items-center">
                          <div id="preview" class="tx-color-03 d-none d-sm-block">
                            <img  src="{{asset("storage/$company->avatar")}}">
                          </div>
                          <div class="media-body mg-sm-l-20">
                            <h4 class="tx-18 tx-sm-20 mg-b-2 tx-uppercase">DATOS DE LA EMPRESA</h4>
                          </div>
                        </div>
                      </div>

                      <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                        <div class="row row-sm">

                          <div class="col-sm">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                            <input type="text" class="form-control" placeholder="NOMBRE" name="name" value="{{$company->name}}">
                          </div>

                          <div class="col-sm-5 mg-t-20 mg-sm-t-0">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">WEB SITE</label>
                            <input type="text" class="form-control" placeholder="WEB SITE" name="website" value="{{$company->website}}">
                          </div>

                          <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ENTIDAD DE ACREDITACIÓN</label>
                            <input type="text" class="form-control" placeholder="ENTIDAD DE ACREDITACIÓN" name="acreditationBody" value="{{$company->acreditation_body}}">
                          </div>

                        <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                          <br>
                            <div class="form-group">
                              <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">LOGO</label>
                              <div class="custom-file">
                              <input type="file" name="img" size="150" maxlength="150" accept="image/jpeg,image/png" class="form-group custom-file-input" id="file" capture="camera"/>
                              <input type="hidden" class="form-control" placeholder="" name="avatar" value="{{$company->avatar}}">
                              <label class="custom-file-label " for="customFile">{{$company->avatar}}</label>
                              </div>
                            </div>
                            <div class="divider-text" style="color:#2c5282"><h6>Ubicación</h6></div>
                              <input type="hidden" class="form-control" placeholder="" name="direction_id" value="{{$company->direction->id}}">
                              <div class="row row-sm" required="required">
                                <div class="col-sm">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALLE</label>
                                  <input type="text" class="form-control" placeholder="CALLE" name="steet" value="{{$company->direction->street}}" required="required">
                                </div>
                                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:EXTERIOR</label>
                                  <input type="text" class="form-control" placeholder="NUM:EXTERIOR" name="numberOut" value="{{$company->direction->external_number}}">
                                </div>
                                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:INTERIOR</label>
                                    <input type="text" class="form-control" placeholder="NUM:INTERIOR" name="numberInt" value="{{$company->direction->internal_number}}" >
                                  </div>
                                  <div class="col-sm-1 mg-t-20 mg-sm-t-0">
                                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CP</label>
                                    <input type="text" class="form-control" placeholder="CP" name="cp" value="{{$company->direction->zip_code}}" required="required">
                                  </div>
                              </div>
                          <br>

                            <div class="row row-sm">
                              <div class="col-sm">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">COLONIA</label>
                                <input type="text" class="form-control" placeholder="COLONIA" name="suburb" value="{{$company->direction->suburb}}"required="required">
                              </div>
                              <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CIUDAD</label>
                                <input type="text" class="form-control" placeholder="CIUDAD" name="city" value="{{$company->direction->city}}"required="required">
                              </div>
                              <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                                <input type="text" class="form-control" placeholder="ESTADO" name="state" value="{{$company->direction->state}}"required="required">
                              </div>
                              <div class="col-sm-2">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">MUNICIPIO</label>
                                <input type="text" class="form-control" placeholder="MUNICIPIO" name="municipality" value="{{$company->direction->municipality}}" required="required">
                              </div>
                              <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PAIS</label>
                                <input type="text" class="form-control" placeholder="PAIS" name="country" value="{{$company->direction->country}}" required="required">
                              </div>
                            </div>

                            <br>

                            <div class="row row-sm">
                                <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">REFERENCIAS</label>
                                    <textarea class="form-control" rows="2" name="reference">{{$company->direction->reference}}</textarea>
                                </div>
                            </div>
                            
                            <div class="divider-text " style="color:#2c5282"><h6>Contacto</h6></div>
                            
                            <div class="row row-sm">
                                <div class="col-sm">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                                  <input type="text" class="form-control" placeholder="NOMBRE" name="contact_name" value="{{$company->direction->contact_name}}" required="required">
                                </div>
                                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PUESTO</label>
                                  <input type="text" class="form-control" placeholder="PUESTO" name="contact_job" value="{{$company->direction->contact_job}}" required="required">
                                </div>
                                <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                                  <input type="text" class="form-control" placeholder="CORREO" name="email" value="{{$company->direction->email}}" required="required">
                                </div>
                                <div class="col-sm-1 mg-t-20 mg-sm-t-0">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">EXTENSION</label>
                                  <input type="text" class="form-control" placeholder="EXTENSION" name="extension" value="{{$company->direction->extension}}">
                                </div>
                                <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CELULAR</label>
                                  <input type="text" class="form-control" placeholder="CELULAR" name="phone" value="{{$company->direction->phone}}" required="required">
                                </div>
                            </div>

                            <div class="divider-text " style="color:#2c5282"><h6>Facturacion</h6></div>

                            <div class="row row-sm">
                                <div class="col-sm">
                                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">RFC</label>
                                  <input type="text" class="form-control" placeholder="RFC" name="rfc" value="{{$company->rfc}}" required="required">
                                </div>
                            </div>

                            <br>

                            <div class="d-flex flex-row-reverse ">
                              <button type="submit" class="btn text-white animated pulse" style="background-color: #3182ce ;"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
                            </div>
                        </div>
                    </form>
                  @endforeach
              </div>
          </div>
        </div>

        <br><br><br>

        <div class="container-fluid d-none">
            <div class="card card-body">
              <div class="container-fluid">
                  <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
                    <div class="media align-items-center">
                      <div  class="tx-color-03 d-none d-sm-block">
                        <span class="tx-color-03 d-none d-sm-block"><i data-feather="archive" class="fas fa-user-plus wd-60 ht-60"></i></span>
                      </div>

                      <div class="media-body mg-sm-l-20">
                        <h4 class="tx-18 tx-sm-20 mg-b-2 tx-uppercase">SECTORES Y NORMAS </h4>
                      </div>

                    </div>
                    <a href="#secoresynormas"  class="btn  btn-dark tx-spacing-1 tx-semibold mg-b-8 col-lg-2 text-white d-none d-sm-none d-md-block" style="background-color: #2c5282" data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo </a>
                  </div>
                    <a href="#secoresynormas"  class="btn  btn-dark tx-spacing-1 tx-semibold mg-b-8 col-lg-2 text-white d-md-none" style="background-color: #2c5282" data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo </a>
                  
                  <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30 table-responsive">
                    <table class="table table-striped table-sm  table-bordered ">
                      <thead>
                        <tr>
                          <th scope="col">Sector</th>
                          <th scope="col">Norma</th>
                          <th scope="col">Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($companysectors as $companysector)
                        <tr>
                          <td>{{$companysector->sector->name}}</td>
                          <td>{{$companysector->norm->name}}</td>
                          <td class="mg-l-15">
                            <button type="button" href="#secoresynormasedit" class="btn btn-light btn-icon" data-toggle="modal" data-animation="effect-sign" data-id="{{$companysector->id}}" data-sector="{{$companysector->sector_id}}"data-norm="{{$companysector->norm_id}}"><i data-feather="edit" style="color:#48bb78" ></i></button>
                            <button type="button" href="#secoresynormasdelete" data-toggle="modal" data-animation="effect-sign" data-id="{{$companysector->id}}" class="btn btn-light btn-icon"><i data-feather="trash-2" style="color: red"></i></button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>

              </div>
            </div>
        </div>

@extends('company.createsectors')
@extends('company.editsectors')
@extends('company.deletesectors')

@endsection




@section('scripts')
      <script>
        document.getElementById("file").onchange = function (e) {
          let reader = new FileReader();
          reader.readAsDataURL(e.target.files[0]);
          reader.onload = function () {
            let preview = document.getElementById("preview"),
              image     = document.createElement("img");
              image.src = reader.result;
              preview.innerHTML = "";
              preview.append(image);
          };
        };

      </script>
      <script>
        let nuevo = function() {
          $("<section/>")                         
             .insertBefore("[name='enviar']")    
             .append($(".inputs").html())        
             .find("button")                     
             .attr("onclick", "eliminar(this)")  
             .attr("class", "btn btn-danger btn-block")  
             .html(`<i class="fas fa-trash-alt"></i> Eliminar`);               
        }
        let eliminar = function(obj) {
          $(obj).closest("section")    
                .remove();   
        }          
      </script>
      <script>
        $('#secoresynormasedit').on('show.bs.modal', function (event) {
            var button    = $(event.relatedTarget) 
            var id        = button.data('id') 
            var sector_id = button.data('sector') 
            var norm_id   = button.data('norm') 
            var modal     = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #sector_id').val(sector_id);
            modal.find('.modal-body #norm_id').val(norm_id);
        });
        $('#secoresynormasdelete').on('show.bs.modal', function (event) {
            var button    = $(event.relatedTarget) 
            var id        = button.data('id') 
            var modal     = $(this)
            modal.find('.modal-body #id').val(id);
        });
      </script>
@endsection 


