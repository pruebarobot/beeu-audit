<div class="modal fade" id="normasysectores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignacion de normas  a un auditor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="post" action="{{ route('updatenorm') }}">
      {{ csrf_field() }}
      <input type="hidden" name="id" id="user_id" value="{{$u->id}}">
      <div class="modal-body">
        <div class="tx-13 mg-b-25">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Sector</th>
                  <th scope="col">Habilitar</th>
                </tr>
              </thead>
              <tbody>
               @foreach($norms as $norm)
                      <tr>
                        <th>{{$norm->name}}</th>
                        <td> 
                            @if(in_array($norm->id,$normIds))
                              <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" id="{{$norm->id}}" value="{{$norm->id}}" name="norm[]" checked disabled>
                                  <label class="custom-control-label" for="{{$norm->id}}"></label>
                              </div> 
                           @else
                              <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" id="norms{{$norm->id}}" value="{{$norm->id}}" name="norm[]">
                                  <label class="custom-control-label" for="norms{{$norm->id}}"></label>
                              </div> 
                           @endif
                        </td>
                      </tr>
                @endforeach

                @foreach($buscadorsector as $sector)
                  <input type="hidden" name="sector[]" value="{{$sector->id}}">
                @endforeach
              </tbody>
            </table>
        </div>
      </div>

      <div class="modal-footer pd-x-20 pd-y-15">
        <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
      </div>
    </form>
    
    </div>
  </div>
</div>