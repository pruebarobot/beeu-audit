@extends('layouts.app')
    @section('css')
        <style type="text/css">
            .avatar{
                width: 60px;
                height:60px;
                border-radius: 13px;
            }
            .imagen img{
                width: 80px !important;
                border-radius: 10px;
              }

        </style>
    @endsection

@section('content')
@foreach($user as $u)

        <div class="modal-content">
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">

            <div class="media align-items-center imagen">
              <img src="{{ asset("storage/$u->avatar")}}">
              <div class="media-body mg-sm-l-20">
                <h4 class="tx-18 tx-sm-20 mg-b-6 mg-l-10">{{$u->name}} {{$u->lastname}} {{$u->secondname}}</h4>
                <h4 class="tx-18 tx-sm-20 mg-b-6 mg-l-10">Codigo Auditor: {{$u->code}}</h4>
              </div>
            </div><!-- media -->

          </div><!-- modal-header -->
          <form class="form-horizontal" method="post" action="{{ route('user.update',$u->id) }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FOTO DE PERFIL</label>

                    <div class="custom-file">
                      <input type="file" name="img" size="150" maxlength="150" accept="image/jpeg,image/png" class="form-group custom-file-input" id="file" capture="camera"/>
                      <label class="custom-file-label" for="customFile">.15789_{{$u->name}}.jpg</label>
                      <input type="hidden" value="{{$u->avatar}}" name="avatar">
                    </div>

                </div>

                <div class="row row-sm">
                    <div class="col-sm">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                      <input type="text" class="form-control" placeholder="" name="name" value="{{$u->name}}" required="required">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO PATERNO</label>
                      <input type="text" class="form-control" placeholder="" name="lastname" value="{{$u->lastname}}" required="required">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO MATERNO</label>
                      <input type="text" class="form-control" placeholder="" name="secondname" value="{{$u->secondname}}"  required="required">
                    </div>
                </div>

                <br>

                <div class="row row-sm">
                    <div class="col-sm">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                      <input type="text" class="form-control" placeholder="Ingrese el email" name="email" value="{{$u->email}}" required="required">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CONTRASEÑA</label>
                      <input type="text" class="form-control" placeholder="********" name="password">
                    </div>
                </div>

                <br>

                <div class="row row-sm">
                    <div class="col-sm">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TELEFONO</label>
                      <input type="text" class="form-control" placeholder="" name="cellphone" value="{{$u->cellphone}}" required="required">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CODIGO</label>
                      <input type="text" class="form-control" placeholder="" name="code" value="{{$u->code}}" required="required">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERFIL</label>
                      <select class="form-control custom-select" name="profile" value="{{$u->profile}}" required="required">
                          <!-- <option disabled>Escoge una opción</option> -->
                          <!-- <option value="Administrador" >Administrador</option> -->
                          <option value="Auditor" >Auditor</option>
                      </select>
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                      <select class="form-control custom-select" name="status" >
                          <option disabled >Escoge una opción</option>
                        @if($u->status=="Activo")
                          <option value="Activo" selected>Activo</option>
                          <option value="Inactivo">Inactivo</option>
                          <option value="Suspendido">Suspendido</option>
                        @elseif($u->status=="Inactivo")
                          <option value="Inactivo" selected>Inactivo</option>
                          <option value="Activo" >Activo</option>
                          <option value="Suspendido">Suspendido</option>
                        @else
                          <option value="Suspendido" selected>Suspendido</option>
                          <option value="Activo">Activo</option>
                          <option value="Inactivo">Inactivo</option>
                        @endif
                      </select>
                    </div>
                </div>
                <br>
                 <div class="modal-footer pd-x-20 pd-y-15">
                  <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Actualizar</button>
                </div>
              </form>
              <hr>
                <div data-label="Example" class="container-fluid">
                  <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="contact-tab3" data-toggle="tab" href="#contact3" role="tab" aria-controls="contact" aria-selected="false">Educación</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="jobs3" data-toggle="tab" href="#job3" role="tab" aria-controls="contact" aria-selected="false">Competencia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#sectores" role="tab" aria-controls="profile" aria-selected="false">Sectores </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#norma" role="tab" aria-controls="profile" aria-selected="false">Normas</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab4" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile4" aria-selected="false">Historial de auditorias</a>
                    </li>
                  </ul>
                  <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent3">
                     <div class="tab-pane fade " id="profile4" role="tabpanel" aria-labelledby="profile-tab3">
                      <h6>Historial de auditorias</h6>
                        <div class="form-group">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FILTRO DE SECTORES</label>
                            <select class="form-control custom-select" id="sectorFiltro">
                                      <option value="">---Selecionar---</option>
                                @foreach($sectors as $sector)
                                      <option value="{{$sector->name}}">{{$sector->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <a href="#modalhistory" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign"><i class="fas fa-user-plus"></i> Agregar mas </a>

                          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">Organizacion</th>
                                  <th scope="col">Inicio</th>
                                  <th scope="col">Fin</th>
                                  <th scope="col">Sector</th>
                                  <th scope="col">Norma</th>
                                  <th scope="col">Rol</th>
                                  <th scope="col"></th>
                                </tr>
                              </thead>
                              <tbody class="parametroshistory">
                                @foreach($u->AuditorHistory as $auditHistory)
                                <tr class="[{{$auditHistory->sector}}] buscadorFiltro">
                                  <td>{{$auditHistory->name_company}}</td>
                                  <td>{{$auditHistory->init_date}}</td>
                                  <td>{{$auditHistory->finish_date}}</td>
                                  <td>{{$auditHistory->sector}}</td>
                                  <td>{{$auditHistory->norm}}</td>
                                  <td>{{$auditHistory->roll}}</td>
                                  <td>
                                    <button type="button" class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#deletehistory" data-animation="effect-sign" data-catid="{{$auditHistory->id}}">
                                      <i data-feather="trash-2" style="color: red"></i>
                                    </button>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="tab-pane fade " id="sectores" role="tabpanel" aria-labelledby="profile-tab3">
                      <h6>Sectores </h6>
                        <a href="#secoresynormas" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign"><i class="fas fa-user-plus"></i> Agregar mas </a>

                          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30" >
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">Sector</th>
                                  <th scope="col">Acciones</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($buscadorsector as $auditorsector)
                                <tr>
                                  <td>{{$auditorsector->name}}</td>
                                  <td>
                                    <!-- <a class=" btn btn-light  btn-icon" data-toggle="tooltip" data-placement="top" title="Editar registro"><i data-feather="edit" style="color:#48bb78" ></i></a> -->
                                    <a data-toggle="modal" href="#delete" data-animation="effect-sign" data-catid="{{$auditorsector->id}}" class="btn btn-light  btn-icon" title="Eliminar registro"  data-toggle="modal" ><i data-feather="trash-2" style="color: red"></i></a>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="tab-pane fade " id="norma" role="tabpanel" aria-labelledby="profile-tab3">
                      <h6>Normas</h6>
                        <a href="#normasysectores" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign"><i class="fas fa-user-plus"></i> Agregar mas </a>

                          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">Norma</th>
                                  <th scope="col">Acciones</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($buscadornorma as $auditorsector)
                                <tr>
                                  <td>{{$auditorsector->name}}</td>
                                  <td>
                                    <!-- <a class=" btn btn-light  btn-icon" data-toggle="tooltip" data-placement="top" title="Editar registro"><i data-feather="edit" style="color:#48bb78" ></i></a> -->
                                    <a data-toggle="modal" href="#deletenorm" data-animation="effect-sign" data-catid="{{$auditorsector->id}}" class="btn btn-light  btn-icon" title="Eliminar registro"  data-toggle="modal" ><i data-feather="trash-2" style="color: red"></i></a>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="tab-pane fade show active" id="contact3" role="tabpanel" aria-labelledby="contact-tab3">
                      <h6>Estudios</h6>
                      <a href="#new" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo</a>
                        <br><br>

                            <div id="accordion7" class="accordion accordion-style2v">
                              @foreach ($academics as $academ)
                              <h6 class="accordion-title">{{$academ->institution}}</h6>
                              <div class="accordion-body">
                                <div class="row">

                                  <div class="col-lg-2 col-md-4 col-sm-12">
                                    <div class="card card-file">
                                      <div class="dropdown-file">
                                          <a href="" class="dropdown-link" data-toggle="dropdown"><i data-feather="more-vertical"></i></a>
                                          <div class="dropdown-menu dropdown-menu-right">
                                            @if($academ->file_path!='Sin Certificado')
                                              <a href="{{asset('/storage/'.$academ->file_path)}}" target="_blank" class="dropdown-item download"><i data-feather="download"></i>Descargar</a>
                                            @else
                                              <label  for="customFileLang"><span   class="dropdown-item download"><i data-feather="share"></i>Subir</span></label>
                                              <input type="hidden" name="courses_id" value="{{$academ->id}}" id="courses_id">
                                            @endif
                                          </div>
                                      </div><!-- dropdown -->
                                      @if(substr($academ->file_path,-4)==".pdf")
                                        <div class="card-file-thumb tx-danger"><i class="far fa-file-pdf"></i></div>
                                      @elseif($academ->file_path=="Sin Certificado")
                                        <div class="card-file-thumb tx-danger" id="upload"><i class="far fa-file-excel" id="removeupload"></i></div>
                                      @else
                                        <div class="card-file-thumb tx-primary"><i class="far fa-file-word"></i></div>
                                      @endif

                                      <div class="card-body">
                                        <h6><a href="" class="link-02">@if($academ->file_path!='Sin Certificado'){{$academ->name}}@else <a id="nofilestitle">NO HAY ARCHIVOS</a> @endif</a></h6>
                                        <span>@if($academ->file_path!='Sin Certificado')100.kb @else @endif</span>
                                      </div>

                                      <div class="card-footer"><span class="d-none d-sm-inline">@if($academ->file_path!='Sin Certificado')Fecha de modificación:{{$academ->created_at}}@else <a id="nofiles">NO HAY ARCHIVOS</a>  @endif </span>
                                      </div>

                                    </div>
                                  </div><!-- col -->

                                    <div class="col-lg-6 col-md-8 col-sm-12 ">
                                      <ul class="list-group">
                                        <li class="list-group-item"><strong>CARRERA/POSGRADO: </strong> {{$academ->carrier}}</li>
                                        <li class="list-group-item"><strong>PERIODO: </strong> {{$academ->date}}</li>
                                        <li class="list-group-item"><strong>CALIFICACIÓN: </strong> {{$academ->score}}</li>
                                        <li class="list-group-item"><strong>TITULO,DIPLOMA.CERTIFICACO,ETC: </strong> {{$academ->certification_institution}}</li>
                                      </ul>
                                      <a data-toggle="modal" href="#deletecourse" data-animation="effect-sign" data-id="{{$academ->id}}" class="btn btn-light  btn-icon" title="Eliminar registro" style="float: right;"  data-toggle="modal" ><i data-feather="trash-2" style="color: red; float: right;"></i> Eliminar</a>
                                    </div>

                              </div>
                            </div>
                          @endforeach
                        </div>

                      </div>
                      <div class="tab-pane fade " id="job3" role="tabpanel" aria-labelledby="job3">
                        <h6>Laboral</h6>
                        <form method="post" action="{{route('newdetailjob')}}">
                            <input type="hidden" name="id" value="{{$user[0]->id}}">
                            {{ csrf_field() }}
                        <table class="table table-striped table-sm">
                            <tr>
                                <th>Periodo</th>
                                <th>Empresa</th>
                                <th>Puesto</th>
                                <th>Actividad desarrollada</th>
                                <th><button type="button" class="btn btn-xs btn-primary jobhistoryadd" style="float:right"><i class="fas fa-plus-circle"></i></button></th>
                            </tr>
                            <tbody class="jobhistory">
                                @foreach($user[0]->JobHistory as $evaluation)  
                                    <tr>
                                        <td><input type="hidden" class="id" value="{{$evaluation->id}}">{{$evaluation->period}}</td>
                                        <td>{{$evaluation->company}}</td>
                                        <td>{{$evaluation->position}}</td>
                                        <td>{{$evaluation->developed_activity}}</td>
                                        <td><button type="button" class="btn btn-danger btn-xs eliminarjobhistory"><i class="fas fa-trash-alt"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                     <!--    <hr>
                        <h6>Auditorías realizadas</h6>
                        <table class="table table-striped table-sm">
                            <tr>
                                <th>Empresa</th>
                                <th>Tipo de auditoria</th>
                                <th>Norma</th>
                                <th>Fecha</th>
                                <th>Días</th>
                                <th>Sector</th>
                                <th><button type="button" class="btn btn-xs btn-primary auditrealizatedadd" style="float:right"><i class="fas fa-plus-circle"></i> </button></th>
                            </tr>
                            <tbody class="auditrealizated">
                                @foreach($user[0]->YearceHistory as $evaluation)  
                                    <tr>
                                        <td><input type="hidden" class="id" value="{{$evaluation->id}}">{{$evaluation->company}}</td>
                                        <td>{{$evaluation->type}}</td>
                                        <td>{{$evaluation->norm}}</td>
                                        <td>{{$evaluation->date}}</td>
                                        <td>{{$evaluation->days}}</td>
                                        <td>{{$evaluation->sector}}</td>
                                        <td><button type="button" class="btn btn-danger btn-xs eliminaryearhistory"><i class="fas fa-trash-alt"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> -->
                        <hr>
                        <h6>Formación</h6>
                        <table class="table table-striped table-sm">
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Horas</th>
                                <th>Fecha</th>
                                <th>Días</th>
                                <th>Impartido por</th>
                                <th><button type="button" class="btn btn-xs btn-primary formationadd" style="float:right"><i class="fas fa-plus-circle"></i> </button></th>
                            </tr>
                            <tbody class="formation">
                                @foreach($user[0]->ExperienceHistory as $evaluation)  
                                    <tr>
                                        <td><input type="hidden" class="id" value="{{$evaluation->id}}"> {{$evaluation->course}}</td>
                                        <td>{{$evaluation->hours}}</td>
                                        <td>{{$evaluation->date}}</td>
                                        <td>{{$evaluation->days}}</td>
                                        <td>{{$evaluation->imparted}}</td>
                                        <td><button type="button" class="btn btn-danger btn-xs eliminarexperiencehistory"><i class="fas fa-trash-alt"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <h6>Evaluación de desempeño</h6>
                        <table class="table table-striped table-sm">
                            <tr>
                                <th>Empresa</th>
                                <th>Fecha</th>
                                <th>Tipo de auditoria</th>
                                <th>Periodo</th>
                                <th>Calificación</th>
                                <th>Testificación</th>
                                <th>Calificación cliente</th>
                                <th><button type="button" class="btn btn-xs btn-primary evaluationadd" style="float:right"><i class="fas fa-plus-circle"></i></button></th>
                            </tr>
                            <tbody class="evaluation">
                                @foreach($user[0]->EvaluationHistory as $evaluation)  
                                    <tr>
                                        <td><input type="hidden" class="id" value="{{$evaluation->id}}">{{$evaluation->company}}</td>
                                        <td>{{$evaluation->date}}</td>
                                        <td>{{$evaluation->type}}</td>
                                        <td>{{$evaluation->period}}</td>
                                        <td>{{$evaluation->qualification}} 
                                          @if($evaluation->qualificationfile)
                                            <a class="btn btn-success btn-xs"  target="_blank" href="{{Storage::url($evaluation->qualificationfile)}}"><i class="fas fa-file-alt"></i></button>
                                          @endif
                                          </td>
                                        <td>{{$evaluation->evaluation}}</td>
                                        <td>{{$evaluation->client_qualification}}
                                          @if($evaluation->client_qualificationfile)
                                             <a class="btn btn-success btn-xs"  target="_blank" href="{{Storage::url($evaluation->client_qualificationfile)}}"><i class="fas fa-file-alt"></i></button>
                                          @endif
                                            </td>
                                        <td><button type="button" class="btn btn-danger btn-xs eliminarevaluationhistory"><i class="fas fa-trash-alt"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <button type="submit" class="btn btn-primary" style="float: right;" > <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
                        <br>
                        </form>
                      </div>
                    </div>
              </div>
        </div><!-- modal-content -->
                <div class="modal-footer pd-x-20 pd-y-15">
                  <a href="{{ route('auditors') }}" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Atras</a>
                </div>


       <form id="frmFormulario" enctype="multipart/form-data d-none">
        <input type="file" class="custom-file-input d-none" id="customFileLang" name="filecourse" lang="es" onchange="fileacademic(this);">
        <input type="hidden" name="course_id" value="" id="course_id">
      </form>

<div id="modalhistory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <div class="media-body mg-sm-l-20">
            <h4 class="tx-18 tx-sm-20 mg-b-2">INGRESE LOS DATOS </h4>
          </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="form-horizontal">
        <form method="post" action="{{route('savehistory')}}">
          {{ csrf_field() }}
          <input type="hidden" class="form-control" name="id" value="{{$user[0]->id}}">

          <div class="modal-body">
            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE DE LA ORGANIZACION</label>
                <input type="text" class="form-control" placeholder="NOMBRE DE LA ORGANIZACION" id="name_organization" name="name_organization" required>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FECHA INICIO</label>
                <input type="date" class="form-control" placeholder="FECHA INICIO" id="date_first" name="date_first" required>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FECHA TERMINO</label>
                <input type="date" class="form-control" placeholder="FECHA TERMINO" id="date_end" name="date_end" required>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">SECTOR</label>
                <select class="form-control custom-select"id="sector" name="sector" required >
                    @foreach($sectors as $sector)
                          <option value="{{$sector->name}}">{{$sector->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NORMA</label>
                <select class="form-control custom-select" id="norma" name="norma" required>
                    @foreach($norms as $norm)
                          <option value="{{$norm->name}}">{{$norm->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ROL</label>
                <input type="text" class="form-control" placeholder="Rol" id="rol" name="rol">
            </div>                       
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" > <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@extends('auditor.course')
@extends('auditor.deletesectorauditor')
@extends('auditor.deletenormauditor')
@extends('auditor.addsectorauditor')
@extends('auditor.addnormauditor')
@extends('auditor.deletecourse')
@extends('auditor.deletehistory')


@endforeach

    @section('scripts')

      <script>
        document.getElementById("file").onchange = function (e) {
              let reader        = new FileReader();
              reader.readAsDataURL(e.target.files[0]);
              reader.onload     = function () {
              let preview       = document.getElementById("preview"),
              image             = document.createElement("img");
              image.src         = reader.result;
              preview.innerHTML = "";
              preview.append(image);
          };
        };
        $('#deletenorm').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var cat_id = button.data('catid')
          var modal  = $(this)
          modal.find('.modal-body #id').val(cat_id);
        })

        $('#delete').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var cat_id = button.data('catid')
          var modal  = $(this)
          modal.find('.modal-body #id').val(cat_id);
        })

        $('#deletecourse').on('show.bs.modal', function (event) {
          var modal = $(event.relatedTarget)
          var id     = modal.data('id')
          var modal  = $(this)
          modal.find('.modal-body #id').val(id);
        })

        $('#deletehistory').on('show.bs.modal', function (event) {
          var modal  = $(event.relatedTarget)
          var id     = modal.data('catid')
          var modal  = $(this)
          modal.find('.modal-body #id').val(id);
        })

      </script>
      <script>
        function fileacademic(sel){
          var gif        = document.getElementById("upload");
          var courses_id = $("#courses_id").val();
          var course_id  = $("#course_id").val(courses_id);
          var parametros = new FormData($('#frmFormulario')[0]);

              $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
              $.ajax({
                  url:'{{route('savefilecourse')}}',
                  data: parametros,
                  method:'POST',
                  processData: false,
                  contentType: false,
                  cache: false,
                success:function(data){
                  gif.innerHTML=`<img src="{{asset('success.png')}}" width="70%"/>`;
                  $("#nofiles").text('SUBIDO CORRECTAMENTE')
                  $("#nofilestitle").text('CON CERTIFICADO')
                },
                error:function(data){
                    console.log(data);
                }
            });
        }


        $('.eliminarjobhistory').on('click', function(event) {
            event.preventDefault();
            var id = $(this).closest('tr').find($('.id')).val() 
            $(this).closest('tr').remove()
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                  url:'{{route('deletejobdetail')}}',
                  data: {id:id,context:1},
                  method:'POST',
                success:function(data){
                    Swal.fire({
                        title: "Datos eliminados",
                        text: "Correctamente",
                        icon: "success",
                        confirmButtonText: "Ok",
                    });
                },
            });
        });
        $('.eliminaryearhistory').on('click', function(event) {
            event.preventDefault();
            var id = $(this).closest('tr').find($('.id')).val() 
            $(this).closest('tr').remove()
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                  url:'{{route('deletejobdetail')}}',
                  data: {id:id,context:2},
                  method:'POST',
                success:function(data){
                    Swal.fire({
                        title: "Datos eliminados",
                        text: "Correctamente",
                        icon: "success",
                        confirmButtonText: "Ok",
                    });
                },
            });
        });
        $('.eliminarexperiencehistory').on('click', function(event) {
            event.preventDefault();
            var id = $(this).closest('tr').find($('.id')).val() 
            $(this).closest('tr').remove()
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                  url:'{{route('deletejobdetail')}}',
                  data: {id:id,context:3},
                  method:'POST',
                success:function(data){
                    Swal.fire({
                        title: "Datos eliminados",
                        text: "Correctamente",
                        icon: "success",
                        confirmButtonText: "Ok",
                    });
                },
            });
        });
        $('.eliminarevaluationhistory').on('click', function(event) {
            event.preventDefault();
            var id = $(this).closest('tr').find($('.id')).val() 
            $(this).closest('tr').remove()
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                  url:'{{route('deletejobdetail')}}',
                  data: {id:id,context:4},
                  method:'POST',
                success:function(data){
                    Swal.fire({
                        title: "Datos eliminados",
                        text: "Correctamente",
                        icon: "success",
                        confirmButtonText: "Ok",
                    });
                },
            });
        });


            $( ".jobhistoryadd" ).click(function() {
                $('.jobhistory').append('<tr><td><input type="text" class="form-control"  name="Periodojobhistory[]"></td><td><input type="text" class="form-control"  name="Empresajobhistory[]"></td><td><input type="text" class="form-control"  name="Puestojobhistory[]"></td><td><input type="text" class="form-control"  name="Actividadjobhistory[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            $( ".auditrealizatedadd" ).click(function() {
                $('.auditrealizated').append('<tr><td><input type="text" class="form-control"  name="Empresaauditrealizated[]"></td><td><input type="text" class="form-control"  name="Tipoauditrealizated[]"></td><td><input type="text" class="form-control"  name="Normaauditrealizated[]"></td><td><input type="date" class="form-control"  name="Fechaauditrealizated[]"></td><td><input type="text" class="form-control"  name="Diasauditrealizated[]"></td><td><input type="text" class="form-control"  name="Sectorauditrealizated[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            $( ".formationadd" ).click(function() {
                $('.formation').append('<tr><td><input type="text" class="form-control"  name="Nombreformation[]"></td><td><input type="text" class="form-control"  name="Horasformation[]"></td><td><input type="date" class="form-control"  name="Fechaformation[]"></td><td><input type="text" class="form-control"  name="Díasformation[]"></td><td><input type="text" class="form-control"  name="Impartidoformation[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            // $( ".evaluationadd" ).click(function() {
            //     $('.evaluation').append('<tr><td><input type="text" class="form-control"  name="Empresaevaluation[]"></td><td><input type="date" class="form-control"  name="Fechaevaluation[]"></td><td><input type="text" class="form-control"  name="Tipoevaluation[]"></td><td><input type="text" class="form-control"  name="Periodoevaluation[]"></td><td><input type="text" class="form-control"  name="Calificacionevaluation[]"><td><input type="text" class="form-control"  name="Testificaciónevaluation[]"><td><input type="text" class="form-control"  name="CalificacioClientenevaluation[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
            //     $( ".deleterow" ).click(function() {
            //         $(this).closest('tr').remove();
            //     });
            // });

            $( ".evaluationadd" ).click(function() {
                $('.evaluation').append('<tr><td><input type="text" class="form-control"  name="Empresaevaluation[]"></td><td><input type="date" class="form-control"  name="Fechaevaluation[]"></td><td><input type="text" class="form-control"  name="Tipoevaluation[]"></td><td><input type="text" class="form-control"  name="Periodoevaluation[]"></td><td><input type="text" class="form-control"  name="Calificacionevaluation[]"><button class="btn btn-xs btn-success btn-block archivocalificacion">Archivo</button><input type="text" name="archivoCalificacion[]" class="calificacionArchivoResultado d-none"><input type="file" class="file d-none" accept="application/pdf, image/jpeg"><td><input type="text" class="form-control"  name="Testificaciónevaluation[]"><td><input type="text" class="form-control"  name="CalificacioClientenevaluation[]"><button class="btn btn-xs btn-success btn-block archivocalificacioncliente">Archivo</button><input type="text"  name="archivoCalificacionCliente[]" class="calificacionArchivoResultadoCliente d-none"><input type="file" class="fileCliente d-none" accept="application/pdf, image/jpeg"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });


                $(".archivocalificacion").click(function(e) {
                    e.preventDefault()
                    $(this).closest('tr').find('.file').trigger('click');
                });

                $(".archivocalificacioncliente").click(function(e) {
                    e.preventDefault()
                    $(this).closest('tr').find('.fileCliente').trigger('click');
                });

                $(".file").change(function() {
                    var $tr = $(this);
                    var formdata = new FormData();
                    var  file = $(this).prop('files')[0];
                    formdata.append("formdata", file);
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
                    $.ajax({
                        url:'{{route('savefilesevaluation')}}',
                        data:formdata,
                        method:'POST',
                        processData: false,
                        contentType: false,
                        cache: false,
                        success:function(data){
                            var $PATH = data;  
                            $tr.closest('tr').find('.calificacionArchivoResultado').val($PATH);
                        },
                        error:function(data){
                            console.log(data);
                        }
                    });
                });

                $(".fileCliente").change(function() {
                    var $tr = $(this);
                    var formdata = new FormData();
                    var  file = $(this).prop('files')[0];
                    formdata.append("formdata", file);
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
                    $.ajax({
                        url:'{{route('savefilesevaluationcliente')}}',
                        data:formdata,
                        method:'POST',
                        processData: false,
                        contentType: false,
                        cache: false,
                        success:function(data){
                            var $PATH = data;  
                            $tr.closest('tr').find('.calificacionArchivoResultadoCliente').val($PATH);
                        },
                        error:function(data){
                            console.log(data);
                        }
                    });
                });
                
            });


            $( "#sectorFiltro" ).change(function() {
                var comparar = $(this).val();
                var $tr      = $(this).closest('body').find('.buscadorFiltro');
                $tr.each(function(index, el) {
                    if($(this).hasClass('['+ comparar +']')){
                        $(this).removeClass('d-none')
                    }else{
                        $(this).addClass('d-none')
                    } 
                });
                
                
            });
      </script>

@endsection




@endsection


