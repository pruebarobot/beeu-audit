<div class="modal fade" id="job" tabindex="-1" role="dialog" aria-hidden="true">
  <form class="form-horizontal" method="post" action="{{ route('auditors.jobupdate',$j->id) }}">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body pd-x-25 pd-sm-x-30 pd-t-40 pd-sm-t-20 pd-b-15 pd-sm-b-20">
          <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </a>

          <div class="nav-wrapper mg-b-20 tx-13">
            <div>
              <nav class="nav nav-line tx-medium">
                <a href="#performance" class="nav-link active" data-toggle="tab">Laboral</a>
                <a href="#rating" class="nav-link" data-toggle="tab">Años de experiencia</a>
                <a href="#activities" class="nav-link" data-toggle="tab">Experiencia Industrial</a>
              </nav>
            </div>
          </div><!-- nav-wrapper -->

          <div class="tab-content">
            {{ csrf_field() }} 
            <div id="performance" class="tab-pane fade active show">
              <label>Laboral :</label>
              <textarea class="form-control" name="namejob"  rows="5" class="ckeditor" id="laboraltext"  >{!! $j->job !!}</textarea>
            </div><!-- tab-pane -->
            <div id="rating" class="tab-pane fade">
              <label>Años de experiencia :</label>
              <input type="text" class="form-control" name="yearsjob" placeholder="Ingrese los años de experiencia" value="{{$j->year_experience}}" >
            </div><!-- tab-pane -->
            <div id="activities" class="tab-pane fade">
              <label>Experiencia Industrial :</label>
              <textarea class="form-control" name="experiencejob"  rows="5" class="ckeditor" id="industrialtext" >{!! $j->industry_experience !!}</textarea>
            </div>
          </div><!-- tab-content -->

        </div><!-- modal-body -->

        <div class="modal-footer pd-x-20 pd-y-15">
          <a href="{{ URL::previous() }}" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Atras</a>
          <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Actualizar</button>
        </div>

      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </form>
</div><!-- modal -->