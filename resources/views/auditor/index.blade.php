@extends('layouts.app')

@section('css')
    <style type="text/css">
        .imagen img{
            width: 80px !important;
            border-radius: 10px;
          }
          .pagination .page-link{
            background-color:#7987a1;
            border: none;
            color: white;
          }
          .pagination  .page-link:hover{
            background-color:#64738f;
            border: none;
            color: white;
          }

          .pagination .disabled{
            display: none;
          }
          .page-item.active .page-link {
            background-color:#3b4863 !important;
            }
    </style>
@endsection

@section('content')
    @if ($errors->any())
      <div class="errors">
          <p><strong>Por favor corrige los siguientes errores<strong></p>
          <ul class="text-danger">
              @foreach ($errors->all() as $error)
                  <li><div class="alert alert-danger" role="alert">{{ $error }}</div></li>
              @endforeach
          </ul>
      </div>
  @endif
   <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Auditores</li>
        </ol>
        <br>
    </nav>

    <div class="col-sm-12 col-lg-12 col-md-12">
        <div class="card card-body">
            <div class="container-fluid ">
                <div class="row">
                    
                <div class="col-md-6">
                    <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10">
                        <i data-feather="users"></i> Auditores
                    </h6>
                </div>
                <div class="col-md-6">
                    <a href="#new"  class="btn  btn-dark tx-spacing-1 tx-semibold col-lg-4 col-md-4 col-sm-12 text-white newbutton" style="float: right;" data-toggle="modal" data-animation="effect-sign" >
                        <i class="fas fa-plus"></i> Nuevo
                    </a>
                </div>
                </div>
            </div>

            <hr>

            <div class="container-fluid row">
                <h1 class="tx-20 tx-spacing-1 tx-color-03 tx-semibold mg-b-8 col-lg-10">Filtros</h1>
            </div>

            <form>
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">SECTOR</label>
                            <select class="custom-select" name="sector">
                                <option selected disabled>Seleccionar...</option>
                                @foreach($sectors as $sector)
                                    <option value="{{$sector->id}}">{{$sector->id}} --- {{$sector->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NORMA</label>
                            <select class="custom-select" name="norms">
                                <option selected disabled>Seleccionar...</option>
                                @foreach($norms as $norm)
                                    <option value="{{$norm->id}}">{{$norm->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE,APELLIDOS O CORREO</label>
                            <span class="form-inline input-group">
                                <input name="search" class="form-control" type="search" placeholder="Buscar por nombre,apellido o correo" aria-label="Search">
                            </span>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <br>
                            <button class="btn bg-dark text-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
                            <button class="btn bg-dark text-white" type="submit"> Ver todos</button>
                        </div>
                        
                    </div>
                </div>

                <hr>
            </form>

            <div class="">
                <div class="table-responsive ">
                    <table class="table table-striped table-sm  table-bordered ">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th class="d-none d-md-table-cell">Apellidos</th>
                                <th class="d-none d-md-table-cell">Correo</th>
                                <th class="d-none d-md-table-cell">Sectores </th>
                                <th class="d-none d-md-table-cell">Normas</th>
                                <th class="d-none d-md-table-cell">Télefono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                                @if($user->profile != "Administrador")
                                    <tr>
                                        <td>{{$user->code}}</td>
                                        <td>{{$user->name}}</td>
                                        <td class="d-none d-md-table-cell">{{$user->lastname}} {{$user->secondname}}</td>
                                        <td class="d-none d-md-table-cell">{{$user->email}}</td>
                                        <td class="d-none d-md-table-cell">
                                            @foreach($user->AuditorSector as $companysector)
                                                <li class="{{$user->id}}{{$companysector->company->sector->id}}Sector">{{$companysector->company->sector->name}}</li>
                                            @endforeach
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            @foreach($user->AuditorSector as $companysector)
                                                <li class="{{$user->id}}{{$companysector->company->norm->id}}Norma">{{$companysector->company->norm->name}}</li>
                                            @endforeach
                                        </td>

                                        <td class="d-none d-md-table-cell">{{$user->cellphone}}</td>
                                        <td>
                                            <a href="editauditor/{{$user->id}}" class="btn btn-light btn-icon">
                                                <i data-feather="edit" style="color:#48bb78" ></i>
                                            </a>
                                            <button class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#delete" data-animation="effect-sign" data-catid="{{$user->id}}">
                                                <i data-feather="trash-2" style="color: red"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>

                @if (count($users))
                    <div name="paginador" style="float: left;">
                        {{ $users->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    @extends('auditor.createauditor')
    @extends('user.deleteusers')

@endsection

@section('scripts')
    <script>
        @foreach($users as $user)
            @foreach($user->AuditorSector as $companysector)
                $('.{{$user->id}}{{$companysector->company->sector->id}}Sector').addClass('d-none');
                $('.{{$user->id}}{{$companysector->company->sector->id}}Sector').first().removeClass('d-none');
                $('.{{$user->id}}{{$companysector->company->norm->id}}Norma').addClass('d-none');
                $('.{{$user->id}}{{$companysector->company->norm->id}}Norma').first().removeClass('d-none');
            @endforeach
        @endforeach
    </script>

    <script>
        $( document ).ready(function() {
            CKEDITOR.replace('laboraltext');
            CKEDITOR.replace('industrialtext');
        });

        $( "#modalhistory #add" ).click(function() {
            var name_organization = $('#name_organizationmh').val();
            var date_first        = $('#date_firstmh').val();
            var date_end          = $('#date_endmh').val();
            var sector            = $('#sectormh').val();
            var norma             = $('#normamh').val();
            var rol               = $('#rolmh').val();

            $('#name_organizationmh').val("");
            $('#date_firstmh').val("");
            $('#date_endmh').val("");
            $('#sectormh').val("");
            $('#normamh').val("");
            $('#rolmh').val("");

            $("#new .parametroshistory").append("<tr><td><input type='text' class='form-control' name='name_organizationhc[]' value='"+name_organization+"' readonly></td><td><input type='text' class='form-control' name='sectorhc[]' value='"+sector+"' readonly></td><td><input type='text' class='form-control' name='normahc[]' value='"+norma+"' readonly></td><td><input type='text' class='form-control' name='rolhc[]' value='"+rol+"' readonly></td><td><button class='btn btn-danger eliminar'>Eliminar</button><input type='hidden' class='form-control' name='date_endhc[]' value='"+date_end+"' readonly><input type='hidden' class='form-control' name='date_firsthc[]' value='"+date_first+"' readonly></td></tr>");

            $('.eliminar').on('click',function(e) {
                $(this).closest('tr').remove();
            });

        });
    </script>
    <script>
      $('#delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var cat_id = button.data('catid')
        var modal  = $(this)
        modal.find('.modal-body #user_id').val(cat_id);
      })
    </script>
    <script>

        $( "#topModal #add" ).click(function() {
            var formname     = $('#topModal #name_course').val();
            var formdata     = $('#topModal #date_course').val();
            var formcual     = $('#topModal #carrier_certification').val();
            var formpass     = $('#topModal #passselect').val();
            var title        = $('#titlecertificate').val();
            var parametros   = new FormData($('#frmFormulario')[0])



            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });

            $.ajax({
                url:'{{route('savefilecourse')}}',
                data:parametros,
                method:'POST',
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    var $PATH = data;  
                    $("#new .parametrosparaenviar").append("<tr><td><input type='text' class='form-control' name='namecourse[]' value='"+formname+"' readonly></td><td><input type='text' name='datecourse[]'class='form-control' value='"+formdata+"' readonly></td><td><button class='btn btn-danger eliminar'>Eliminar</button><input type='hidden' name='scorecourse[]' value='"+formpass+"'><input type='hidden' name='passcourse[]' value='"+formcual+"'><input type='hidden' value='"+$PATH+"' name='filecourse[]'><input type='hidden' value='"+title+"' name='titlecertificate[]'></td></tr>");
                    $('#name_course').val("");
                    $('#date_course').val("");
                    $('#qualifselect').val("");
                    $('#passselect').val("");
                    $('#frmFormulario').val("");
                    $('#carrier_certification').val("");
                    $('#titlecertificate').val("");

                    $('.eliminar').on('click',function(e) {
                        $(this).closest('tr').remove();
                    });
                },
                error:function(data){
                    console.log(data);
                }
            });

        });
    </script>

    <script>
        document.getElementById("file").onchange = function (e) {
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);

            reader.onload     = function () {
                let preview       = document.getElementById("preview"),
                image             = document.createElement("img");
                image.src         = reader.result;
                preview.innerHTML = "";

                preview.append(image);
            };
        };
    </script>

    <script>
        $(function(){
            'use strict'
            $('#wizard2').steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                onFinishing: function (event, currentIndex, priorIndex) {
                    $("#select_academic").after('<button class="bnt btn-primary btn-block" onclick="otheracademic(this);"><i class="fas fa-plus-circle"></i> Agregar otro</button><br><div class="alert alert-success" role="alert">Agregado<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    $('#wizard2 .steps a:eq(0)').click();
                    $('#wizard2').hide();

                    var miSelect = $('#qualifselect');
                    miSelect.val(miSelect.children('option:first').val());

                    $('#name_course').val('');
                    $('#date_course').val('');
                    $('#passselect').val('');
                    $('#customFileLang').val('');
                    $('#qualifselect').prop('disabled', false);
                    $('#name_course').prop('disabled', false);
                    $('#date_course').prop('disabled', false);
                    $('#passselect').prop('disabled', false);
                    $('#customFileLang').prop('disabled', false);
                },
                labels:{
                    finish: "Agregar",
                    previous: "Anterior",
                    next: "Siguiente",
                },
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>'
            });
        });
    </script>

    <script type="text/javascript">
        (function($, window) {
            'use strict';
            var MultiModal = function(element) {
                this.$element = $(element);
                this.modalCount = 0;
            };

            MultiModal.BASE_ZINDEX = 1040;

            MultiModal.prototype.show = function(target) {
                var that = this;
                var $target = $(target);
                var modalIndex = that.modalCount++;

                $target.css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20) + 10);

                // Bootstrap triggers the show event at the beginning of the show function and before
                // the modal backdrop element has been created. The timeout here allows the modal
                // show function to complete, after which the modal backdrop will have been created
                // and appended to the DOM.
                window.setTimeout(function() {
                    // we only want one backdrop; hide any extras
                    if(modalIndex > 0)
                        $('.modal-backdrop').not(':first').addClass('hidden');

                    that.adjustBackdrop();
                });
            };

            MultiModal.prototype.hidden = function(target) {
                this.modalCount--;

                if(this.modalCount) {
                   this.adjustBackdrop();

                    // bootstrap removes the modal-open class when a modal is closed; add it back
                    $('body').addClass('modal-open');
                }
            };

            MultiModal.prototype.adjustBackdrop = function() {
                var modalIndex = this.modalCount - 1;
                $('.modal-backdrop:first').css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20));
            };

            function Plugin(method, target) {
                return this.each(function() {
                    var $this = $(this);
                    var data = $this.data('multi-modal-plugin');

                    if(!data)
                        $this.data('multi-modal-plugin', (data = new MultiModal(this)));

                    if(method)
                        data[method](target);
                });
            }

            $.fn.multiModal = Plugin;
            $.fn.multiModal.Constructor = MultiModal;

            $(document).on('show.bs.modal', function(e) {
                $(document).multiModal('show', e.target);
            });

            $(document).on('hidden.bs.modal', function(e) {
                $(document).multiModal('hidden', e.target);
            });
}(jQuery, window));


            $( ".jobhistoryadd" ).click(function() {
                $('.jobhistory').append('<tr><td><input type="text" class="form-control"  name="Periodojobhistory[]"></td><td><input type="text" class="form-control"  name="Empresajobhistory[]"></td><td><input type="text" class="form-control"  name="Puestojobhistory[]"></td><td><input type="text" class="form-control"  name="Actividadjobhistory[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            $( ".auditrealizatedadd" ).click(function() {
                $('.auditrealizated').append('<tr><td><input type="text" class="form-control"  name="Empresaauditrealizated[]"></td><td><input type="text" class="form-control"  name="Tipoauditrealizated[]"></td><td><input type="text" class="form-control"  name="Normaauditrealizated[]"></td><td><input type="date" class="form-control"  name="Fechaauditrealizated[]"></td><td><input type="text" class="form-control"  name="Diasauditrealizated[]"></td><td><input type="text" class="form-control"  name="Sectorauditrealizated[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            $( ".formationadd" ).click(function() {
                $('.formation').append('<tr><td><input type="text" class="form-control"  name="Nombreformation[]"></td><td><input type="text" class="form-control"  name="Horasformation[]"></td><td><input type="date" class="form-control"  name="Fechaformation[]"></td><td><input type="text" class="form-control"  name="Díasformation[]"></td><td><input type="text" class="form-control"  name="Impartidoformation[]"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });
            });

            $( ".evaluationadd" ).click(function() {
                $('.evaluation').append('<tr><td><input type="text" class="form-control"  name="Empresaevaluation[]"></td><td><input type="date" class="form-control"  name="Fechaevaluation[]"></td><td><input type="text" class="form-control"  name="Tipoevaluation[]"></td><td><input type="text" class="form-control"  name="Periodoevaluation[]"></td><td><input type="text" class="form-control"  name="Calificacionevaluation[]"><button class="btn btn-xs btn-success btn-block archivocalificacion">Archivo</button><input type="text" name="archivoCalificacion[]" class="calificacionArchivoResultado d-none"><input type="file" class="file d-none" accept="application/pdf, image/jpeg"><td><input type="text" class="form-control"  name="Testificaciónevaluation[]"><td><input type="text" class="form-control"  name="CalificacioClientenevaluation[]"><button class="btn btn-xs btn-success btn-block archivocalificacioncliente">Archivo</button><input type="text"  name="archivoCalificacionCliente[]" class="calificacionArchivoResultadoCliente d-none"><input type="file" class="fileCliente d-none" accept="application/pdf, image/jpeg"></td><td><button class="btn btn-danger btn-xs deleterow"><i class="fas fa-trash-alt"></i></button></td></tr>');
                $( ".deleterow" ).click(function() {
                    $(this).closest('tr').remove();
                });


                $(".archivocalificacion").click(function(e) {
                    e.preventDefault()
                    $(this).closest('tr').find('.file').trigger('click');
                });

                $(".archivocalificacioncliente").click(function(e) {
                    e.preventDefault()
                    $(this).closest('tr').find('.fileCliente').trigger('click');
                });

                $(".file").change(function() {
                    var $tr = $(this);
                    var formdata = new FormData();
                    var  file = $(this).prop('files')[0];
                    formdata.append("formdata", file);
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
                    $.ajax({
                        url:'{{route('savefilesevaluation')}}',
                        data:formdata,
                        method:'POST',
                        processData: false,
                        contentType: false,
                        cache: false,
                        success:function(data){
                            var $PATH = data;  
                            $tr.closest('tr').find('.calificacionArchivoResultado').val($PATH);
                        },
                        error:function(data){
                            console.log(data);
                        }
                    });
                });

                $(".fileCliente").change(function() {
                    var $tr = $(this);
                    var formdata = new FormData();
                    var  file = $(this).prop('files')[0];
                    formdata.append("formdata", file);
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
                    $.ajax({
                        url:'{{route('savefilesevaluationcliente')}}',
                        data:formdata,
                        method:'POST',
                        processData: false,
                        contentType: false,
                        cache: false,
                        success:function(data){
                            var $PATH = data;  
                            $tr.closest('tr').find('.calificacionArchivoResultadoCliente').val($PATH);
                        },
                        error:function(data){
                            console.log(data);
                        }
                    });
                });
                
            });
                  


    </script>
@endsection
