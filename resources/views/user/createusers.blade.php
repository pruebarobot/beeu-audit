    <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">

            <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </a>

            <div class="media align-items-center">
                <div id="preview" class="imagen text-center">
                  <span class="tx-color-03 d-none d-sm-block"><i data-feather="user-plus" class="fas fa-user-plus wd-60 ht-60"></i></span>
                </div>

              <div class="media-body mg-sm-l-20">
                <h4 class="tx-18 tx-sm-20 mg-b-2">INGRESE LOS DATOS DEL USUARIO</h4>
              </div>
            </div><!-- media -->

          </div><!-- modal-header -->
          <form class="form-horizontal" method="post" action="{{ route('saveuser') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">

            <div class="form-group">
              <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FOTO DE PERFIL</label>
                <div class="custom-file">
                    <input type="file" name="foto" size="150" maxlength="150"  accept="image/jpeg,image/png" class="form-group custom-file-input" id="file" capture="camera">
                <label class="custom-file-label" for="customFile">Escoger Foto</label>
              </div>
            </div>

            <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                  <input type="text" class="form-control" placeholder="NOMBRE" name="nombre" required="required" value="{{ old('nombre') }}">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO PATERNO</label>
                  <input type="text" class="form-control" placeholder="APELLIDO PATERNO" name="apellidomaterno" required="required" value="{{ old('apellidomaterno') }}">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO MATERNO</label>
                  <input type="text" class="form-control" placeholder="APELLIDO MATERNO" name="apellidopaterno" required="required" value="{{ old('apellidopaterno') }}">
                </div>
            </div>

            <div class="row row-sm mt-3">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                  <input type="email" class="form-control" placeholder="INGRESE EL CORREO" name="email" required="required" value="{{ old('email') }}">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CONTRASEÑA</label>
                  <input type="password" class="form-control" placeholder="********" name="password" required="required" value="{{ old('password') }}">
                </div>
            </div>

            <div class="row row-sm mt-3">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TÉLEFONO</label>
                  <input type="number" class="form-control" placeholder="TÉLEFONO" name="telefono" required="required" value="{{ old('telefono') }}">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CÓDIGO</label>
                  <input type="text" class="form-control" placeholder="CÓDIGO" name="codigo" required="required" value="{{ old('codigo') }}">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERFIL</label>
                  <select class="form-control custom-select" name="perfil" >
                      <option value="Comercial">Comercial</option>
                      <option value="Planeador">Planeador</option>
                      <!-- <option value="Registro">Registro</option> -->
                      <!-- <option value="Tecnico">Técnico</option> -->
                      <option value="Administrador">Administrador</option>
                  </select>
                </div>

                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                  <select class="form-control custom-select" name="status" required="required" value="{{ old('status') }}">
                      <option value="Activo" >Activo</option>
                      <option value="Inactivo">Inactivo</option>
                      <option value="Suspendido">Suspendido</option>
                  </select>
                </div>

            </div>

          </div>
          <div class="modal-footer pd-x-20 pd-y-15">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>
        </form>
    </div>
  </div>
</div>


