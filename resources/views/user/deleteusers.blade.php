<div class="modal" tabindex="-1" id="delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <form action="{{route('user.delete')}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}

            <div class="modal-body">
                <h4>Estas seguro de eliminar el registro.</h4>
                <input type="hidden" name="user_id" id="user_id" value="">
            </div>
          
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">No, Cancelar</button>
                <button type="submit" class="btn btn-danger"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Si, Eliminar</button>
            </div>
      </form>
      
    </div>
  </div>
</div>
