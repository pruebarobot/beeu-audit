@extends('layouts.app')
    @section('css')
        <style type="text/css">
            .avatar{
                width: 60px;
                height:60px;
                border-radius: 13px;
            }
            .imagen img{
                width: 80px !important;
                border-radius: 10px;
              }
            .container-fluid {
              /* width: 100%; */
              padding-right: 0px;
              padding-left: 0px;
              margin-right: auto;
              margin-left: auto;
          }
          hr{
            width: 90%;
            background-color: #4D6F35 !important;
          }


        </style>
    @endsection

@section('content')
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Auditoria</li>
          </ol>
          <br>
        </nav>
        <div class="col-sm-6 col-lg-12">
          <div class="card card-body">
            <div class="container-fluid row">
            <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-20 col-lg-10"><i data-feather="clipboard"></i> Auditoria</h6>
            <a href="#modalBillingInfo" data-toggle="modal" class="btn btn-light green-btn  tx-spacing-1 tx-semibold mg-b-8 col-lg-2">
              <i class="fas fa-user-plus"></i> Nuevo 
            </a>
            </div>
            <center>
            <hr>
            </center>
                <div class="table table-striped ">
                  <table class="table mg-b-0" id="table">
                    <thead>
                     <tr class="bg-greay">
                      <th class="wd-20p">Fecha</th>
                      <th class="wd-20p">Organización</th>
                      <th class="wd-25p">Folio</th>
                      <th class="wd-20p">Tipo</th>
                      <th class="wd-20p">Encargado</th>
                      <th class="wd-20p">Estado</th>
                      <th class="wd-15p">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                          <tr>
                              <td class=""></td>
                              <td class=""></td>
                              <td class=""></td>
                              <td class=""></td>
                              <td class=""></td>
                              <td class=""></td>

                              
                              <td class=""> 
                                <a href=""class=" btn btn-light info-btn btn-icon" data-toggle="tooltip" data-placement="top" title="Editar registro"><i class="fa fa-edit"></i></a>
                                <a class=" btn btn-light danger-btn btn-icon" data-toggle="tooltip" data-placement="top" title="Eliminar registro"><i class="fa fa-trash"></i></a>
                              </td>
                          </tr>
                    </tbody>
                  </table>
          </div>
        </div>
     </div>



@endsection

@section('scripts')

      <script>
        $( document ).ready(function() {
            CKEDITOR.replace('laboraltext');
            CKEDITOR.replace('industrialtext');

        });   
      </script>
@endsection 


