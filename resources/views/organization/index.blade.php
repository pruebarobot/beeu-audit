@extends('layouts.app')
    @section('css')
        <style type="text/css">
            .imagen img{
                    width: 80px !important;
                    border-radius: 10px;
                }
                .pagination .page-link{
                    background-color:#7987a1;
                    border: none;
                    color: white;
                }
                .pagination  .page-link:hover{
                    background-color:#64738f;
                    border: none;
                    color: white;
                }

                .pagination .disabled{
                    display: none;
                }
                .page-item.active .page-link {
                    background-color:#3b4863 !important;
                }

        </style>
    @endsection
@section('content')
                @if ($errors->any())
                    <div class="errors">
                            <p><strong>Por favor corrige los siguientes errores<strong></p>
                            <ul class="text-danger">
                                    @foreach ($errors->all() as $error)
                                            <li><div class="alert alert-danger" role="alert">{{ $error }}</div></li>
                                    @endforeach
                            </ul>
                    </div>
            @endif
             <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-style1 mg-b-0">
                        <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Auditores</li>
                    </ol>
                    <br>
                </nav>


                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="card card-body">
                        <div class="container-fluid">
                            <div class="row">
                                    <div class="col-md-6">
                                        <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="briefcase"></i> Organizacion</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#new"  class="btn  btn-dark tx-spacing-1 tx-semibold col-lg-4 col-md-4 col-sm-12 text-white newbutton" style="float: right;" data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo</a>
                                    </div>
                            </div>
                        </div>
                        <hr>
                        <div class="container-fluid row">
                                <h1 class="tx-20 tx-spacing-1 tx-color-03 tx-semibold mg-b-8 col-lg-10">Filtros</h1>
                        </div>

                        <form>
                        <div class="container-fluid row">
                            <div class="col-lg-4 col-sm-12 col-md-12">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">SECTOR</label>
                                <select class="custom-select" name="sectors">
                                    <option selected disabled>Seleccionar...</option> 
                                    @foreach($sectors as $sector)
                                            <option value="{{$sector->id}}">{{$sector->id}} ---  {{$sector->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-md-12">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NORMA</label>
                                <select class="custom-select" name="norms">
                                    <option selected disabled>Seleccionar...</option>
                                        @foreach($norms as $norm)
                                                <option value="{{$norm->id}}">{{$norm->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-md-12">
                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE,RFC o CÓDIGO</label>
                                <input name="search" class="form-control" type="search" placeholder="Buscar por nombre,rfc o código" aria-label="Search">
                            </div>
                        </div>
                        <div class="container-fluid">
                                <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                                <br>
                                                <button class="btn bg-dark text-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
                                                <button class="btn bg-dark text-white" type="submit"> Ver todos</button>
                                        </div>
                                </div>
                        </div>
                                <hr>
                            </form>
                        <div class="container-fluid ">
                                <div class="table-responsive ">
                                    <table class="table table-striped table-sm  table-bordered ">
                                        <thead>
                                         <tr>
                                            <th>Código</th>
                                            <th>Organizacion</th>
                                            <th class="d-none d-md-table-cell">RFC</th>
                                            <th class="d-none d-md-table-cell">Sector </th>
                                            <th class="d-none d-md-table-cell">Norma</th>
                                            <th class="d-none d-md-table-cell">Contacto</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                @foreach($organizations as $organization)
                                                    <tr>
                                                        <td>{{$organization->code}}</td>
                                                        <td>{{$organization->name}}</td>
                                                        <td class="d-none d-md-table-cell">{{$organization->rfc}}</td>
                                                        <td class="d-none d-md-table-cell">
                                                            @foreach($organization->organizationSectorNorm as $companiesSector)
                                                                <li class="{{$organization->id}}{{$companiesSector->companies->sector->id}}Sector ">{{$companiesSector->companies->sector->name}}</li>
                                                            @endforeach
                                                        </td>
                                                        <td class="d-none d-md-table-cell">
                                                            @foreach($organization->organizationSectorNorm as $companiesSector)
                                                                <li class="{{$organization->id}}{{$companiesSector->companies->norm->id}}Norm ">{{$companiesSector->companies->norm->name}}</li>
                                                            @endforeach
                                                        </td>

                                                        <td class="d-none d-md-table-cell">{{$organization->directionCertification->phone}}</td>
                                                            <td> 
                                                                <a href="editorganization/{{$organization->id}}" class="btn btn-light btn-icon" ><i data-feather="edit" style="color:#48bb78" ></i></a>
                                                                <button class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#delete" data-animation="effect-sign" id="deleteorganizatio" data-id="{{$organization->id}}" ><i data-feather="trash-2" style="color: red"></i></button>
                                                            </td>
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @if (count($organizations))
                                        <div name="paginador" style="float: left;">
                                                {{ $organizations->links() }}
                                        </div>
                                @endif
                    </div>
                </div>
         </div>
@extends('organization.addorganization')
@extends('organization.deleteorganization')

@endsection

@section('scripts')

<script>
@foreach($organizations as $organization)
    @foreach($organization->organizationSectorNorm as $companiesSector)
            $('.{{$organization->id}}{{$companiesSector->companies->sector->id}}Sector').addClass('d-none');
            $('.{{$organization->id}}{{$companiesSector->companies->sector->id}}Sector').first().removeClass('d-none');
            $('.{{$organization->id}}{{$companiesSector->companies->norm->id}}Norm').addClass('d-none');
            $('.{{$organization->id}}{{$companiesSector->companies->norm->id}}Norm').first().removeClass('d-none');
        @endforeach
    @endforeach
</script>

<script type="text/javascript">
    
    jQuery(document).ready(function($) {
        $("#invoice").click(function() {
             if ($(this).is(":checked")){
                    $("#streets").val($("#calle").val()).prop("disabled", true);
                    $("#numberOuts").val($("#exterior").val()).prop("disabled", true);
                    $("#numberInts").val($("#interior").val()).prop("disabled", true);
                    $("#cps").val($("#cp").val()).prop("disabled", true);
                    $("#suburbs").val($("#colonia").val()).prop("disabled", true);
                    $("#citys").val($("#ciudad").val()).prop("disabled", true);
                    $("#states").val($("#estado").val()).prop("disabled", true);
                    $("#municipalitys").val($("#municipio").val()).prop("disabled", true);
                    $("#countrys").val($("#pais").val()).prop("disabled", true);
                    $("#references").val($("#reference").val()).prop("disabled", true);
             }else{
                    $("#streets").val(" ").prop("disabled", false);
                    $("#numberOuts").val(" ").prop("disabled", false);
                    $("#numberInts").val(" ").prop("disabled", false);
                    $("#cps").val(" ").prop("disabled", false);
                    $("#suburbs").val(" ").prop("disabled", false);
                    $("#citys").val(" ").prop("disabled", false);
                    $("#states").val(" ").prop("disabled", false);
                    $("#municipalitys").val(" ").prop("disabled", false);
                    $("#countrys").val(" ").prop("disabled", false);
                    $("#references").val(" ").prop("disabled", false);
             } 
        });
    });
                $('#delete').on('show.bs.modal', function (event) {
                    var button    = $(event.relatedTarget) 
                    var id        = button.data('id') 
                    var modal     = $(this)
                    modal.find('.modal-body #id').val(id);
            });


    $(document).ready(function() {
        $("#searchOrganizationSectorNorm").on("keyup", function() {
            if($(this).val().length>=1){
                var valor = capitalize($(this).val())
                $('#secnor').find("tr").addClass('d-none');
                $('#secnor').find("tr>th:contains("+ valor +")").closest("tr").removeClass('d-none');
            }else{
                $('#secnor').find("tr").removeClass('d-none');
            }
        });

        $("#searchOrganizationSectorNormEdit").on("keyup", function() {
            if($(this).val().length>=1){
                var valor = capitalize($(this).val())
                $('#editsector').find("tr").addClass('d-none');
                $('#editsector').find("tr>th:contains("+ valor +")").closest("tr").removeClass('d-none');
            }else{
                $('#editsector').find("tr").removeClass('d-none');
            }
        });

        function capitalize(word) {
          return word[0].toUpperCase() + word.slice(1);
        }
    });
</script>

@endsection 


