@extends('layouts.app')

@section('content')
@foreach($organization as $u)
        <div class="modal-content">
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
            <div class="container-fluid row">
            <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="briefcase"></i> {{$u->name}}</h6>
            </div>
            <div class="media align-items-center imagen">
            </div><!-- media -->
          </div><!-- modal-header -->
          <form class="form-horizontal" method="post" action="{{route('updateorganization')}}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
          <div class="divider-text " style="color:#2c5282"><h6>Organizacion</h6></div>
            <input type="hidden" class="form-control" placeholder="" name="id" value="{{$u->id}}">
            <div class="row row-sm">
              <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CÓDIGO</label>
                  <input type="text" class="form-control" placeholder="CÓDIGO" name="codigo" required="required" value="{{$u->code}}">
              </div>

              <div class="col-sm">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                <input type="text" class="form-control" placeholder="NOMBRE" name="name" value="{{$u->name}}" required="required">
              </div>
              <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">RFC</label>
                    <input type="text" class="form-control" placeholder="RFC" name="rfc" value="{{$u->rfc}}" required="required">
                  </div>
              </div>
            </div>
            <br>
            <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NO. EMPLEADOS</label>
                    <input type="text" class="form-control" placeholder="NO. EMPLEADOS" name="sitios" required="required" value="{{$u->employees}}">
                  </div>
             <div class="row row-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NO. SITIOS</label>
                    <input type="text" class="form-control" placeholder="NO. SITIOS" name="empleados" required="required" value="{{$u->sites}}">
              </div>                            
            </div>
            <br>
            <div class="divider-text" style="color:#2c5282"><h6>Ubicación</h6></div>
            <input type="hidden" name="idcertification" value="{{$u->directionCertification->id}}">
              <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALLE</label>
                  <input type="text" class="form-control" placeholder="CALLE" name="steet" value="{{$u->directionCertification->street}}" id="street" required="required">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:EXTERIOR</label>
                  <input type="text" class="form-control" placeholder="NUM:EXTERIOR" name="numberOut" value="{{$u->directionCertification->external_number}}" id="numberOut" required="required">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:INTERIOR</label>
                    <input type="text" class="form-control" placeholder="NUM:INTERIOR" name="numberInt" value="{{$u->directionCertification->internal_number}}" id="numberInt" required="required">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CP</label>
                    <input type="text" class="form-control" placeholder="CP" name="cp" value="{{$u->directionCertification->zip_code}}" id="cp" required="required">
                  </div>
              </div>
              <br>
            <div class="row row-sm">
              <div class="col-sm">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">COLONIA</label>
                <input type="text" class="form-control" placeholder="COLONIA" name="suburb" value="{{$u->directionCertification->suburb}}" id="suburb" required="required">
              </div>
              <div class="col-sm-6 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CIUDAD</label>
                <input type="text" class="form-control" placeholder="CIUDAD" name="city" value="{{$u->directionCertification->city}}" id="city" required="required">
              </div>
            </div>
            <br>
            <div class="row row-sm">
              <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                <input type="text" class="form-control" placeholder="ESTADO" name="state" value="{{$u->directionCertification->state}}" id="state" required="required">
              </div>
              <div class="col-sm-4">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">MUNICIPIO</label>
                <input type="text" class="form-control" placeholder="MUNICIPIO" name="municipality" value="{{$u->directionCertification->municipality}}" id="municipality" required="required">
              </div>
              <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PAIS</label>
                <input type="text" class="form-control" placeholder="PAIS" name="country" value="{{$u->directionCertification->country}}" id="country" required="required">
              </div>
            </div>
              <br>
            <div class="row row-sm">
                <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">REFERENCIAS</label>
                    <textarea class="form-control" rows="2" name="reference" id="reference" placeholder="REFERENCIAS"> {{$u->directionCertification->reference}}</textarea>
                </div>
            </div>
            <br>
            <div class="col-sm-12 mg-t-20 mg-sm-t-0" >
              <div class="custom-control custom-switch">
                @if($u->directionInvoice->street==$u->directionCertification->street)
                  <input type="checkbox" class="custom-control-input" id="invoice" name="invoice" value="1" checked>
                @else
                  <input type="checkbox" class="custom-control-input" id="invoice" name="invoice" value="1">
                @endif
                <label class="custom-control-label" for="invoice">El Domicilio es el mismo a facturar</label>
              </div>          
            </div>
            <br>
            <div class="divider-text" style="color:#2c5282"><h6>Domicilio Fiscal</h6></div>
            <input type="hidden" name="idinvoice" value="{{$u->directionInvoice->id}}">
              <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALLE</label>
                    <input type="text" class="form-control" placeholder="CALLE" name="steets" value="{{$u->directionInvoice->street}}" id="streets" required="required">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:EXTERIOR</label>
                    <input type="text" class="form-control" placeholder="NUM:EXTERIOR" name="numberOuts" value="{{$u->directionInvoice->external_number}}" id="numberOuts" required="required">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:INTERIOR</label>
                      <input type="text" class="form-control" placeholder="NUM:INTERIOR" name="numberInts" value="{{$u->directionInvoice->internal_number}}" id="numberInts" required="required">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CP</label>
                      <input type="text" class="form-control" placeholder="CP" name="cps" value="{{$u->directionInvoice->zip_code}}" id="cps" required="required">
                  </div>
              </div>
              <br>
            <div class="row row-sm">
              <div class="col-sm">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">COLONIA</label>
                  <input type="text" class="form-control" placeholder="COLONIA" name="suburbs" value="{{$u->directionInvoice->suburb}}" id="suburbs" required="required">
              </div>
              <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CIUDAD</label>
                  <input type="text" class="form-control" placeholder="CIUDAD" name="citys" value="{{$u->directionInvoice->city}}" id="citys" required="required">
              </div>
            </div>
            <br>
            <div class="row row-sm">
              <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                  <input type="text" class="form-control" placeholder="ESTADO" name="states" value="{{$u->directionInvoice->state}}" id="states" required="required">
              </div>
              <div class="col-sm-4">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">MUNICIPIO</label>
                  <input type="text" class="form-control" placeholder="MUNICIPIO" name="municipalitys" value="{{$u->directionInvoice->municipality}}" id="municipalitys" required="required">
              </div>
              <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PAIS</label>
                  <input type="text" class="form-control" placeholder="PAIS" name="countrys" value="{{$u->directionInvoice->country}}" id="countrys" required="required">
              </div>
            </div>
              <br>
            <div class="row row-sm">
                <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">REFERENCIAS</label>
                    <textarea class="form-control" rows="2" name="references" id="references" placeholder="REFERENCIAS">{{$u->directionInvoice->reference}}</textarea>
                </div>
            </div>          
            <div class="divider-text " style="color:#2c5282"><h6>Contacto</h6></div>
              <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">A CARGO</label>
                    <input type="text" class="form-control" placeholder="A CARGO" name="contact_name" value="{{$u->directionCertification->contact_name}}" required="required">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PUESTO</label>
                    <input type="text" class="form-control" placeholder="PUESTO" name="contact_job" value="{{$u->directionCertification->contact_job}}" required="required">
                  </div>
                  <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                    <input type="text" class="form-control" placeholder="CORREO" name="email" value="{{$u->directionCertification->email}}" required="required">
                  </div>
                  <div class="col-sm-1 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">EXTENSION</label>
                    <input type="text" class="form-control" placeholder="EXTENSION" name="extension" value="{{$u->directionCertification->extension}}">
                  </div>
                  <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TEL MÓVIL</label>
                    <input type="text" class="form-control" placeholder="TEL MÓVIL" name="phone" value="{{$u->directionCertification->phone}}" required="required">
                  </div>
              </div>
          </div>                  
                
                <br>
                <div data-label="Example" class="container-fluid">
                  <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="contact-tab3" data-toggle="tab" href="#contact3" role="tab" aria-controls="contact" aria-selected="true">Sectores</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="contact-tab3" data-toggle="tab" href="#norms3" role="tab" aria-controls="contact" aria-selected="true">Normas</a>
                    </li>                    
                  </ul>
                  <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent3">
                    <div class="tab-pane fade show active" id="contact3" role="tabpanel" aria-labelledby="contact-tab3">
                      <h6>Sectores de la Organizacion</h6>
                      <a href="#secoresynormas" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Agregar otra</a>
                        <br><br>
                          <div class="table-responsive ">
                            <table class="table table-striped table-sm  table-bordered ">
                              <thead>
                               <tr>
                                <th class="d-none d-md-table-cell"></th>
                                <th class="d-none d-md-table-cell">Sector</th>
                                <th>Acciones</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($buscadorsector as $organizationSector)
                                  <tr>
                                    <td>{{$organizationSector->id}}</td>
                                    <td>{{$organizationSector->name}}</td>
                                    <td>
                                      <a data-toggle="modal" href="#delete" id="deletesectororganization" data-animation="effect-sign" data-id="{{$organizationSector->id}}" class="btn btn-light  btn-icon" title="Eliminar registro"  data-toggle="modal" ><i data-feather="trash-2" style="color: red"></i></a>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                      </div>

                      <div class="tab-pane fade " id="norms3" role="tabpanel" aria-labelledby="contact-tab3">
                      <h6>Normas de la Organizacion</h6>
                      <a href="#secoresynormasdos" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton"  data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Agregar otra</a>
                        <br><br>
                          <div class="table-responsive ">
                            <table class="table table-striped table-sm  table-bordered ">
                              <thead>
                               <tr>
                                <th class="d-none d-md-table-cell">Norma</th>
                                <th>Acciones</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($buscadornorma as $organizationSector)
                                  <tr>
                                    <td>{{$organizationSector->name}}</td>
                                    <td>
                                      <a data-toggle="modal" href="#deletenorm" id="deletesectororganization" data-animation="effect-sign" data-id="{{$organizationSector->id}}" class="btn btn-light  btn-icon" title="Eliminar registro"  data-toggle="modal" ><i data-feather="trash-2" style="color: red"></i></a>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                      </div>
                    </div>
              </div>
          <div class="modal-footer pd-x-20 pd-y-15">
            <a href="{{ route('organization') }}" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Atras</a>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Actualizar</button>
          </div>

          <div class="modal fade" id="secoresynormas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Asignacion de normas y sectores</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <input type="hidden" name="id" id="user_id" value="{{$u->id}}">
                <div class="modal-body" style="height: 600px; overflow-y: scroll;">
                  <div class="tx-13 mg-b-25">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Id</th>
                          <th scope="col">Sector</th>
                          <th scope="col">Habilitar</th>
                        </tr>
                      </thead>
                      <tbody id="editsector">
                      @foreach($sectors as $companysector)
                              <tr>
                                <th>{{$companysector->id}}</th>
                                <th>{{$companysector->name}}</th>
                                <td> 
                                  @if(in_array($companysector->id,$sectorIds))
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="{{$companysector->id}}" value="{{$companysector->id}}" checked disabled>
                                          <label class="custom-control-label" for="{{$companysector->id}}"></label>
                                      </div>                          
                                  @else
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="sectores{{$companysector->id}}" value="{{$companysector->id}}" name="companysector[]" >
                                          <label class="custom-control-label" for="sectores{{$companysector->id}}"></label>
                                      </div>  
                                  @endif
                                </td>
                              </tr>
                        @endforeach
                        @foreach($buscadornorma as $sector)
                          <input type="hidden" name="companynormSector[]" value="{{$sector->id}}">
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="modal fade" id="secoresynormasdos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Asignacion de normas y sectores</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <input type="hidden" name="id" id="user_id" value="{{$u->id}}">
                <div class="modal-body">
                  <div class="tx-13 mg-b-25">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Norma</th>
                          <th scope="col">Habilitar</th>
                        </tr>
                      </thead>
                      <tbody id="editsector">
                          @foreach($norms as $companysector)
                              <tr>
                                <th>{{$companysector->name}}</th>
                                <td> 
                                  @if(in_array($companysector->id,$normIds))
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="norma{{$companysector->id}}" value="{{$companysector->id}}" checked disabled>
                                          <label class="custom-control-label" for="norma{{$companysector->id}}"></label>
                                      </div>                          
                                  @else
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="norma{{$companysector->id}}" value="{{$companysector->id}}" name="companynorm[]" >
                                          <label class="custom-control-label" for="norma{{$companysector->id}}"></label>
                                      </div>  
                                  @endif
                                </td>
                              </tr>
                          @endforeach
                          @foreach($buscadorsector as $sector)
                              <input type="hidden" name="companysectorNorm[]" value="{{$sector->id}}">
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </form>
        </div><!-- modal-content -->
       <form id="frmFormulario" enctype="multipart/form-data d-none">
        <input type="file" class="custom-file-input d-none" id="customFileLang" name="filecourse" lang="es" onchange="fileacademic(this);">
        <input type="hidden" name="course_id" value="" id="course_id">
      </form>

@extends('organization.deletesector')
@extends('organization.deletenorm')
@endforeach

    @section('scripts')

      <script>
        $('#delete').on('show.bs.modal', function (event) {
            var button    = $(event.relatedTarget) 
            var id        = button.data('id') 
            var modal     = $(this)
            modal.find('.modal-body #id').val(id);
        });

        $('#deletenorm').on('show.bs.modal', function (event) {
            var button    = $(event.relatedTarget) 
            var id        = button.data('id') 
            var modal     = $(this)
            modal.find('.modal-body #id').val(id);
        });

        $("#searchOrganizationSectorNormEdit").on("keyup", function() {
            if($(this).val().length>=1){
                var valor = capitalize($(this).val())
                $('#editsector').find("tr").addClass('d-none');
                $('#editsector').find("tr>th:contains("+ valor +")").closest("tr").removeClass('d-none');
            }else{
                $('#editsector').find("tr").removeClass('d-none');
            }
        });

        function capitalize(word) {
          return word[0].toUpperCase() + word.slice(1);
        }
      </script>

@endsection 




@endsection


